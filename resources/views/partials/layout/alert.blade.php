<div class="alert alert-{{$type}} mb-0" role="alert">
    <div class="container">
        {{$message}}
    </div>
</div>
