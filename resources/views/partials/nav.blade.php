<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Demo') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
                {!!MenuHelper::menu('default', 'navbar')!!}
            </ul>
            <ul class="navbar-nav ml-auto">
                @if (FeatureHelper::enabled('ecommerce', config('site.features')))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('ecommerce.basket')}}"><i class="fas fa-shopping-cart"></i> {{ __('Basket') }} ({{BasketHelper::itemCount(request()->cookie('identifier'))}} {{__('items')}})</a>
                    </li>
                @endif
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbar-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->email }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-dropdown">
                            @if (Auth::user()->hasAnyRole('Administrator', 'Editor', 'Merchant'))
                                <a class="dropdown-item" href="{{ route('nova.login') }}">
                                    {{ __('Admin') }}
                                </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('account') }}">
                                {{ __('Account') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('account.addresses') }}">
                                {{ __('Addresses') }}
                            </a>                            
                            <a 
                                class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                            >
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>