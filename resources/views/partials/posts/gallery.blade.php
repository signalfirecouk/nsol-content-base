@if ($post->pictures()->count() > 0)
    @php 
        $cardsPerDeck = 3;
        $items = $post->pictures()->orderBy('sort_order')->get()->chunk($cardsPerDeck);
    @endphp
    @foreach($items as $item)
        <div class="card-deck mb-4">
        @foreach($item as $picture)
            <div class="card card-3">
                <a href="#"
                    data-toggle="modal"
                    data-target="#gallery-modal"
                    data-filename="{{route('image', ['filename' => $picture->filename, 'width' => 640, 'height' => 480])}}"
                    data-caption="{{$picture->caption}}"
                    >
                    <img 
                        src="{{route('image', ['filename' => $picture->filename, 'width' => 220, 'height' => 220])}}"
                        alt="{{$picture->caption}}" 
                        class="img-r"
                    />
                </a>
            </div>        
        @endforeach
        </div>
    @endforeach
    <div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img class="modal-image img-r" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>    
    @section('scripts')
        <script nonce="{{ csp_nonce() }}">
            jQuery('#gallery-modal').on('show.bs.modal', function (event) {
                var button = jQuery(event.relatedTarget);
                var filename = button.data('filename');
                var caption = button.data('caption');
                var modal = jQuery(this)
                modal.find('.modal-image').attr('src', filename);
                modal.find('.modal-image').attr('title', caption);
            });
        </script>
    @endsection
@endif

