<aside class="sidebar sidebar-search">
    <form action="/blog/search" method="GET">
        <h3>{{__('Search')}}</h3>
        <div class="form-group">
            <input
                name="keywords"
                type="search"
                class="form-control mb-3"
                placeholder="{{__('Enter search terms...')}}"
                value="{{isset($keywords) ? strip_tags($keywords) : ''}}"
            />
            <input
                type="submit"
                class="btn btn-primary btn-block"
                value="{{__('Search')}}"
            />
        </div>
    </form>
</aside>