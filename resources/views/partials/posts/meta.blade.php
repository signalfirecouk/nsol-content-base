<p class="mb-0">
    <small>
    {{__('Published At')}}: {{$post->created_at->format('d/m/Y')}}@if($post->user->nickname) | {{__('Written By')}}: <a href="{{route('posts.author', ['slug' => $post->user->nickname])}}">{{$post->user->forename}} {{$post->user->surname}}</a>@endif
    </small>
</p>

@foreach($post->tags()->get() as $tag)
    @if ($loop->first)<ul class="lists-flat lists-tags lists-tags-inline mb-3"><li><small>{{__('Tagged')}}:</small></li>@endif 
    <li><small><a href="{{route('posts.tag', ['slug' => $tag->slug])}}">{{$tag->name}}</a></small></li>
    @if ($loop->last)</ul>@endif 
@endforeach

