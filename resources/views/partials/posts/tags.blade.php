<aside class="sidebar sidebar-tags">
    <h3>{{__('Tags')}}</h3>
    @forelse ($tags as $tag)
        @if($loop->first)
            <ul class="lists-flat lists-tags">
        @endif
        @php
            $total = $tag->posts()->count();
        @endphp
        @if ($total > 0)
            <li><a href="{{route('posts.tag', ['slug' => $tag->slug])}}">{{$tag->name}}</a> ({{$total}})</li>
        @endif
        @if($loop->last)
            </ul>
        @endif
    @empty
        <p>{{__('Sorry, no tags')}}</p>
    @endforelse
</aside>