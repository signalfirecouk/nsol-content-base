@if($post->pictures()->count() > 0)
    @php $picture = $post->pictures()->orderBy('sort_order')->first(); @endphp
    @if ($picture)
        <a href="{{route('posts.single', ['slug' => $post->slug])}}" title="{{$post->title}}">
            <img
                src="{{route('image', ['filename' => $picture->filename, 'width' => 750, 'height' => 300])}}"
                alt="{{$picture->caption}}" title="{{$picture->caption}}"
                class="img-r mb-3"/>
        </a>
    @endif
@endif