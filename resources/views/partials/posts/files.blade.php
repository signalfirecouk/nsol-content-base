@if ($post->files()->count() > 0)
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>{{__('Type')}}</th>
                <th>{{__('File Description')}}</th>
                <th>{{__('Size')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($post->files()->get() as $file)
                <tr>
                    <td class="align-middle text-center"><i class="{{ ContentHelper::getFontAwesomeClass($file->filename) }}"/></td>
                    <td class="align-middle">{{ $file->description }}</td>
                    <td class="align-middle">{{ FileHelper::filesizeHuman($file->filename) }}</td>
                    <td class="align-middle">
                        <a href="{{ route('download', ['id' => $file->id]) }}" class="btn btn-sm btn-block btn-primary">{{__('Download')}}</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif