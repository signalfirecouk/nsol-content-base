<article>
    @include('partials.posts.picture')
    <h2><a href="{{route('posts.single', ['slug' => $post->slug])}}">{{$post->title}}</a></h2>
    @include('partials.posts.meta')
    @if($post->excerpt)
        <p>{{$post->excerpt}}</p>
    @else
        <p>{{Str::words(strip_tags($post->content), 40, '...')}}</p>
    @endif
    <p><a href="{{route('posts.single', ['slug' => $post->slug])}}" class="btn btn-primary">{{__('More')}}...</a></p>
</article>