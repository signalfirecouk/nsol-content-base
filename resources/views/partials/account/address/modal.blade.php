<div class="modal fade" id="address-modal" tabindex="-1" aria-labelledby="address-modal-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="address-modal-label">Choose Address</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <select id="address-modal-results" size="5" class="form-control"></select>
          <p id="address-modal-error" class="hidden">Sorry, an error has occurred. Unable to lookup address.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button id="address-modal-use" type="button" class="btn btn-primary">Use Address</button>
        </div>
      </div>
    </div>
</div>