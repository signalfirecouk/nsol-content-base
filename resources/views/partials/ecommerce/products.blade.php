@if (request()->get('display', 'grid') === 'grid')
    @php 
        $cardsPerDeck = 3;
        $items = $products->chunk($cardsPerDeck);
    @endphp
    @forelse($items as $item)
        <div class="card-deck mb-4">
        @foreach($item as $product)
            @include('partials.ecommerce.grid.teaser')
        @endforeach
        </div>
    @empty
        <p>{{__('Sorry, no products')}}</p>
    @endif
@else
    @forelse($products as $product)
        @include('partials.ecommerce.list.teaser')
    @empty
        <p>{{__('Sorry, no products')}}</p>
    @endif
@endif
{{$products->withQueryString()->links()}}