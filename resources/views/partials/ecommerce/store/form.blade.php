<div class="form-group row">
    <label for="store" class="col-md-4 col-form-label text-md-right">Store</label>
    <div class="col-md-6">
        <select id="store" name="store" class="form-control @error('store') is-invalid @enderror">
            <option value="">Choose option...</option>
            @foreach($stores as $store)
                <option value="{{$store->id}}">{{$store->name}}</option>
            @endforeach
        </select>
        @error('store')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror    
    </div>
</div>