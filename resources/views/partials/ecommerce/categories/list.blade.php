<aside class="sidebar sidebar-categories">
    <h3>{{__('Categories')}}</h3>
    @foreach($categories as $category)
        @if ($loop->first) <ul class="lists-flat lists-categories"> @endif
        <li>
            <a href="{{route('ecommerce.category', ['slug' => $category->slug])}}">{{$category->name}}</a>
        </li>
        @if ($loop->last) </ul> @endif
    @endforeach
</aside>