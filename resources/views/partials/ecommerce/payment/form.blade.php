<form id="payment-form">
    <div id="card-element"></div>
    <button id="submit" class="btn btn-primary">
      <div class="spinner hidden" id="spinner"></div>
      <span id="button-text">Make Payment</span>
    </button>
    <p id="card-error" role="alert"></p>                    
</form>