<div class="form-group row">
    <label for="{{$prefix}}-address" class="col-md-4 col-form-label text-md-right">{{ __(':prefix address', ['prefix' => ucfirst($prefix)]) }}</label>

    <div class="col-md-6">
        <select id="{{$prefix}}-address" class="form-control address-selector @error($prefix .'-address') is-invalid @enderror" name="{{$prefix}}-address" data-prefix="{{$prefix}}">
            <option value="00000000-0000-0000-0000-000000000000" {{old($prefix . '-address') === '00000000-0000-0000-0000-000000000000' ? 'selected': ''}}>Add New Address</option>
            @if ($prefix === 'delivery')
                <option value="00000000-0000-0000-0000-000000000001" {{old($prefix . '-address') === '00000000-0000-0000-0000-000000000001' ? 'selected': ''}}>Use Payment Address</option>
                @if(FeatureHelper::enabled('clickandcollect', config('site.features.ecommerce')))
                <option value="00000000-0000-0000-0000-000000000002" {{old($prefix . '-address') === '00000000-0000-0000-0000-000000000002' ? 'selected': ''}}>Use Click And Collect</option>
                @endif
            @endif
            @foreach(request()->user()->addresses()->get() as $address)
                <option value="{{$address->id}}" {{old($prefix . '-address') === $address->id ? 'selected': ''}}>{{$address->postcode}}</option>
            @endforeach
        </select>
        @error($prefix . '-address')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
<div id="{{$prefix}}-fields">
    <div class="form-group row">
        <label for="{{$prefix}}-forename" class="col-md-4 col-form-label text-md-right">{{ __('Forename') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-forename" type="text" class="form-control @error($prefix . '-forename') is-invalid @enderror" name="{{$prefix}}-forename" value="{{ old($prefix . '-forename') }}">

            @error($prefix . '-forename')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div> 
    <div class="form-group row">
        <label for="{{$prefix}}-surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-surname" type="text" class="form-control @error($prefix . '-surname') is-invalid @enderror" name="{{$prefix}}-surname" value="{{ old($prefix . '-surname') }}">

            @error($prefix . '-surname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row">
        <label for="{{$prefix}}-address-1" class="col-md-4 col-form-label text-md-right">{{ __('Address 1') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-address-1" type="text" class="form-control @error($prefix . '-address-1') is-invalid @enderror" name="{{$prefix}}-address-1" value="{{ old($prefix . '-address-1') }}">

            @error($prefix . '-address-1')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="{{$prefix}}-address-2" class="col-md-4 col-form-label text-md-right">{{ __('Address 2') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-address-2" type="text" class="form-control @error($prefix . '-address-2') is-invalid @enderror" name="{{$prefix}}-address-2" value="{{ old($prefix . '-address-2') }}">

            @error($prefix . '-address-2')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row">
        <label for="{{$prefix}}-address-3" class="col-md-4 col-form-label text-md-right">{{ __('Address 3') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-address-3" type="text" class="form-control @error($prefix . '-address-3') is-invalid @enderror" name="{{$prefix}}-address-3" value="{{ old($prefix . '-address-3') }}">

            @error($prefix . '-address-3')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="{{$prefix}}-address-4" class="col-md-4 col-form-label text-md-right">{{ __('Address 4') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-address-4" type="text" class="form-control @error($prefix . '-address-4') is-invalid @enderror" name="{{$prefix}}-address-4" value="{{ old($prefix . '-address-4') }}">

            @error($prefix . '-address-4')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="{{$prefix}}-towncity" class="col-md-4 col-form-label text-md-right">{{ __('Town / City') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-towncity" type="text" class="form-control @error($prefix . '-towncity') is-invalid @enderror" name="{{$prefix}}-towncity" value="{{ old($prefix . '-towncity') }}">

            @error($prefix . '-towncity')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row">
        <label for="{{$prefix}}-county" class="col-md-4 col-form-label text-md-right">{{ __('County') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-county" type="text" class="form-control @error($prefix . '-county') is-invalid @enderror" name="{{$prefix}}-county" value="{{ old($prefix . '-county') }}">

            @error($prefix . '-county')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row">
        <label for="{{$prefix}}-postcode" class="col-md-4 col-form-label text-md-right">{{ __('Postcode') }}</label>

        <div class="col-md-4">
            <input id="{{$prefix}}-postcode" type="text" class="form-control @error($prefix . '-postcode') is-invalid @enderror" name="{{$prefix}}-postcode" value="{{ old($prefix . '-postcode') }}">
            @error($prefix . '-postcode')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-md-2">
            <button id="{{$prefix}}-find-address" type="button" class="btn btn-primary btn-block find-address" data-prefix="{{$prefix}}">Find Address</button>
        </div>
    </div> 
    <div class="form-group row">
        <label for="{{$prefix}}-country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

        <div class="col-md-6">
            {{-- <input id="{{$prefix}}-country" type="text" class="form-control @error($prefix . '-country') is-invalid @enderror" name="{{$prefix}}-country" value="{{ old($prefix . '-country') }}"> --}}
            <select id="{{$prefix}}-country" class="form-control @error($prefix . '-country') is-invalid @enderror" name="{{$prefix}}-country">
                @foreach($countries as $country)
                    <option value="{{$country->name}}" {{old($prefix . '-country') === $country->name ? 'selected': ''}}>{{$country->name}}</option>
                @endforeach
            </select>

            @error($prefix . '-country')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row">
        <label for="{{$prefix}}-telephone" class="col-md-4 col-form-label text-md-right">{{ __('Telephone') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-telephone" type="text" class="form-control @error($prefix . '-telephone') is-invalid @enderror" name="{{$prefix}}-telephone" value="{{ old($prefix . '-telephone') }}">

            @error($prefix . '-telephone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div> 
    <div class="form-group row">
        <label for="{{$prefix}}-mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

        <div class="col-md-6">
            <input id="{{$prefix}}-mobile" type="text" class="form-control @error($prefix . '-mobile') is-invalid @enderror" name="{{$prefix}}-mobile" value="{{ old($prefix . '-mobile') }}">

            @error($prefix . '-mobile')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>