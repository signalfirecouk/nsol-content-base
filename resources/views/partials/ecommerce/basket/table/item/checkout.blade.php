<tr>
    <td>{{$item->product->title}}<br/><small>{{$item->option->title}}</small></td>
    <td class="align-middle">{{BasketHelper::valueAsCurrency(BasketHelper::price($item), config('app.locale'), config('nova.currency'))}}</td>
    <td class="align-middle">{{$item->quantity}}</td>
    <td class="align-middle">{{BasketHelper::valueAsCurrency(BasketHelper::subtotal($item), config('app.locale'), config('nova.currency'))}}</td>
    <td class="align-middle">
        <a href="{{route('ecommerce.checkout.increase', ['id' => $item->id])}}" class="btn btn-primary btn-sm text-white"><i class="fas fa-plus"></i></a>
        <a href="{{route('ecommerce.checkout.decrease', ['id' => $item->id])}}" class="btn btn-primary btn-sm text-white"><i class="fas fa-minus"></i></a>
        <a href="{{route('ecommerce.checkout.remove', ['id' => $item->id])}}" class="btn btn-danger btn-sm text-white"><i class="fas fa-trash"></i></a>
    </td>
</tr>
