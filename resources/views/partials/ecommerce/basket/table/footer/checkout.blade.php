@if($loop->last)
    <tr>
        <td colspan="3"><strong>{{__('Tax')}}</strong></td>
        <td>{{BasketHelper::valueAsCurrency(BasketHelper::taxes($items), config('app.locale'), config('nova.currency'))}}</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"><strong>{{__('Total')}}</strong></td>
        <td>{{BasketHelper::valueAsCurrency(BasketHelper::total($items, BasketHelper::taxes($items)), config('app.locale'), config('nova.currency'))}}</td>
        <td></td>
    </tr>
@endif