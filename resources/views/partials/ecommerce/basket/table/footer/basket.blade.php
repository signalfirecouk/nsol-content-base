@if($loop->last)
    <tr>
        <td colspan="3"><strong>{{__('Tax')}}</strong></td>
        <td>{{BasketHelper::valueAsCurrency(BasketHelper::taxes($items), config('app.locale'), config('nova.currency'))}}</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3"><strong>{{__('Total')}}</strong></td>
        <td>{{BasketHelper::valueAsCurrency(BasketHelper::total($items, BasketHelper::taxes($items)), config('app.locale'), config('nova.currency'))}}</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="5">
            <a href="{{route('ecommerce.checkout')}}" class="btn btn-primary mr-2">{{__('Checkout')}}</a>
            <a href="{{route('ecommerce.basket.clear')}}" class="btn btn-secondary">{{__('Clear Basket')}}</a>
        </td>
    </tr>
@endif
