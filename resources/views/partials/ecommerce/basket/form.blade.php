<form method="POST" action="{{route('ecommerce.basket.add.post')}}" novalidate>
    @csrf
    <div class="form-group">
        <label for="option">Option</label>
        <select id="option" name="option" class="form-control @error('option') is-invalid @enderror">
            @foreach($product->options()->live()->hasStock()->get()->sortBy('purchase_price') as $option)
                <option value="{{$option->id}}" data-stock="{{$option->stock}}" data-price="{{BasketHelper::valueAsCurrency(ProductHelper::price($option), config('app.locale'), config('nova.currency'))}}">{{$option->title}}</option>
            @endforeach
        </select>
        @error('option')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <select id="quantity" name="quantity" class="form-control @error('quantity') is-invalid @enderror"></select>
        @error('quantity')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror        
    </div>
    <div class="mb-3">
        <div id="price"></div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">Add To Basket</button>
    </div>
</form>