<ul class="nav nav-pills mb-4">
    <li class="nav-item">
        @php
            $query = [];
            if(request()->has('sort')) {
                $query['sort'] = request()->get('sort');                
            }
            $query['page'] = request()->get('page', 1);
        @endphp        
        <a 
            class="nav-link{{request()->get('display', 'grid') === 'grid' ? ' active' : ''}}"
            href="?display=grid&{{http_build_query($query)}}">
                <i class="fas fa-th"></i> {{__('Grid')}}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link{{request()->get('display', 'grid') === 'list' ? ' active' : ''}}"
            href="?display=list&{{http_build_query($query)}}">
                <i class="fas fa-th-list"></i> {{__('List')}}
        </a>
    </li>
</ul>