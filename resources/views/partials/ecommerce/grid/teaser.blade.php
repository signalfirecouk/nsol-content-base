@php
    $option = ProductHelper::firstOption($product);
@endphp
<div class="card card-3">
    <img src="{{route('image', ['filename' => ProductHelper::picture($product), 'width' => 250, 'height' => 150])}}" class="card-img-top" alt="{{$product->title}}">
    <div class="card-body">
        <h2 class="card-title">
            <a href="{{route('ecommerce.single', ['slug' => $product->slug])}}">
                {{$product->title}}
            </a>
        </h2>
        <p class="card-text">        
            {{$product->summary}}
        </p>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            @if(ProductHelper::isOnSale($option)) <span class="badge badge-danger">{{__('Sale')}}</span> @endif
            @if(ProductHelper::optionCount($product) > 1) {{__('From')}}: @endif
            @if(ProductHelper::isOnSale($option)) <strike>{{BasketHelper::valueAsCurrency($option->price, config('app.locale'), config('nova.currency'))}}</strike> @endif {{BasketHelper::valueAsCurrency(ProductHelper::price($option), config('app.locale'), config('nova.currency'))}}
        </li>
    </ul>    
    <div class="card-footer">
        @if (ProductHelper::optionCount($product) === 1)
            <a href="{{route('ecommerce.basket.add.get', ['id' => $product->options()->first()->id, 'quantity' => 1])}}" class="btn btn-primary btn-block">{{__('Add To Basket')}}</a>
        @else
            <a href="{{route('ecommerce.single', ['slug' => $product->slug])}}" class="btn btn-primary btn-block">{{__('More Details')}}</a>
        @endif
    </div>
</div>