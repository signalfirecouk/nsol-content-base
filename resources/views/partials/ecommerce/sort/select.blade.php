  <form id="sort" method="GET">
    <input type="hidden" name="display" value="{{request()->get('display', 'grid')}}"/>
    <select name="sort" class="form-control">
        <option value="">Sort by...</option>
        <option value="price-asc" {{request()->get('sort') === 'price-asc' ? 'selected' : ''}}>{{__('Price ASC')}}</option>
        <option value="price-desc" {{request()->get('sort') === 'price-desc' ? 'selected' : ''}}>{{__('Price DESC')}}</option>
    </select>
    <input type="hidden" name="page" value="{{request()->get('page', 1)}}"/>
</form>

@section('scripts')
    <script nonce="{{ csp_nonce() }}">
        jQuery('#sort select').on('change', function(){
            if ($(this).val() === '') {
                window.location.href = '/shop';
            }else{
                $('#sort').submit();
            }
        })
    </script>
@endsection