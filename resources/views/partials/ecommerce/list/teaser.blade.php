<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title">{{$product->title}}</h2>
        <p class="card-text">{{$product->summary}}</p>
        @if ($product->options()->count() === 1)
            <a href="{{route('ecommerce.basket.add.get', ['id' => $product->options()->first()->id, 'quantity' => 1])}}" class="btn btn-primary">{{__('Add To Basket')}}</a>
        @else
            <a href="{{route('ecommerce.single', ['slug' => $product->slug])}}" class="btn btn-primary">{{__('More Details')}}</a>
        @endif
    </div>
</div>