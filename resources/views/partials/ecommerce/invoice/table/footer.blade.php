@if($loop->last)
    <tr>
        <th colspan="2">{{__('Sub Total')}}</td>
        <td colspan="2">{{BasketHelper::valueAsCurrency($invoice->subtotal, config('app.locale'), config('nova.currency'))}}</td>
    </td>
    @if ($invoice->deliveryAddress()->exists())
        <tr>
            <th colspan="2">{{__('Delivery')}}</td>
            <td colspan="2">{{BasketHelper::valueAsCurrency($invoice->delivery, config('app.locale'), config('nova.currency'))}}</td>
        </td>
    @endif
    <tr>
        <th colspan="2">{{__('Tax')}}</td>
        <td colspan="2">{{BasketHelper::valueAsCurrency($invoice->tax, config('app.locale'), config('nova.currency'))}}</td>
    </td>
    <tr>
        <th colspan="2">{{__('Total')}}
        <td colspan="2">{{BasketHelper::valueAsCurrency($invoice->total, config('app.locale'), config('nova.currency'))}}</td>
    </tr>
@endif