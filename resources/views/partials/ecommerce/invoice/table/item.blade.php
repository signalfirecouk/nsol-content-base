<tr>
    <td>{{$item->product->title}}<br><small>{{$item->option->title}}</small></td>
    <td>{{BasketHelper::valueAsCurrency($item->price, config('app.locale'), config('nova.currency'))}}</td>
    <td>{{$item->quantity}}</td>
    <td>{{BasketHelper::valueAsCurrency($item->price * $item->quantity, config('app.locale'), config('nova.currency'))}}</td>
</tr>