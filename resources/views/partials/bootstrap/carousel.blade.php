@if ($pictures->count() > 0)
    <div id="gallery-carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($pictures as $picture)
                @if($loop->first)
                    <li data-target="#gallery-carousel" data-slide-to="{{$loop->index}}" class="active"></li>
                @else
                    <li data-target="#gallery-carousel" data-slide-to="{{$loop->index}}"></li>
                @endif
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($pictures as $picture)
                @if($loop->first)            
                    <div class="carousel-item active">
                @else
                    <div class="carousel-item">
                @endif
                <img src="{{route('image', ['filename' => $picture->filename, 'width' => 730, 'height' => 380])}}" class="d-block w-100" alt="{{$picture->caption}}">
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#gallery-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">{{__('Previous')}}</span>
        </a>
        <a class="carousel-control-next" href="#gallery-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">{{__('Next')}}</span>
        </a>
    </div>
@endif