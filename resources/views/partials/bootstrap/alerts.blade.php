@if(session('status'))
    @include('partials.layout.alert', ['type' => 'success', 'message' => session('status')])
@endif
@if(session('warning'))
    @include('partials.layout.alert', ['type' => 'warning', 'message' => session('warning')])
@endif
@if(session('error'))
    @include('partials.layout.alert', ['type' => 'danger', 'message' => session('error')])
@endif