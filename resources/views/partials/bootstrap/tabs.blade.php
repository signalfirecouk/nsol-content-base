<ul class="nav nav-tabs" id="{{$id}}" role="tablist">
    @foreach($tabs as $tab)
        <li class="nav-item" role="presentation">
            <a class="nav-link {{$loop->first ? 'active' : ''}}" id="{{$tab->slug}}-tab" data-toggle="tab" href="#{{$tab->slug}}" role="tab" aria-controls="{{$tab->slug}}" aria-selected="true">{{ucfirst($tab->title)}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content" id="{{$id}}-tabs">
    @foreach($tabs as $tab)     
        <div class="tab-pane fade show {{$loop->first ? 'active' : ''}}" id="{{$tab->slug}}" role="tabpanel" aria-labelledby="{{$tab->slug}}-tab">{{$tab->content}}</div>
    @endforeach
</div>