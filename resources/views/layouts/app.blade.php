<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', config('app.name', 'Nova Solutions Content Base'))</title>
        <meta name="description" content="@yield('description', config('meta.description', 'Nova Solutions Content Base Description'))"/>
        <script src="{{ config('services.fontawesome.url')}}" crossorigin="anonymous" nonce="{{ csp_nonce() }}"></script>
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('styles')
    </head>
    <body>
        @include('partials.nav')
        @yield('banner')
        @yield('messages')
        <main class="py-4">
            @yield('content')
        </main>
        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <h4>Sitemap</h4>
                            {!!MenuHelper::menu('default', 'default', ['lists-flat', 'lists-menu', 'lists-menu-light'])!!}
                        </div>
                        <div class="col-12 col-md-4">
                            <h4>Contact</h4>
                            {!!ContentHelper::blockBySlug('address')!!}
                        </div>
                        <div class="col-12 col-md-4">
                            <h4>Socials</h4>
                            <ul class="lists-flat lists-menu lists-menu-social-light">
                                @if (SettingHelper::get('facebook_url', null))
                                    <li>
                                        <a href="{{SettingHelper::get('facebook_url')}}" target="_blank" rel="noopener">
                                            <i class="fab fa-facebook-square"></i><span>{{__('Facebook')}}</span>
                                        </a>
                                    </li>
                                @endif
                                @if (SettingHelper::get('twitter_url', null)) 
                                    <li>
                                        <a href="{{SettingHelper::get('twitter_url')}}" target="_blank" rel="noopener">
                                            <i class="fab fa-twitter-square"></i><span>{{__('Twitter')}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6">{!!ContentHelper::blockBySlug('copyright')!!}</div>
                        <div class="col-12 col-md-6">{!!MenuHelper::menu('footer', 'default', ['lists-flat', 'lists-menu','lists-menu-inline', 'lists-menu-divider', 'lists-menu-light', 'text-right'])!!}</div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="{{ asset('js/app.js') }}" nonce="{{ csp_nonce() }}"></script>
        @yield('scripts')
    </body>
</html>
