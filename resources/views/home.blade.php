@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Homepage</h1>
                {!!ContentHelper::blockBySlug('homepage')!!}
            </div>
        </div>
    </div>
@endsection
