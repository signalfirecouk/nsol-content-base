@extends('layouts.app')

@section('content')
    <section class="container page">
        <div class="row">
            <div class="col">
                <h1>{{__('An error has occurred')}}...</h1>
                <p>{{__('Sorry, the page cannot be delivered (500).')}}</p>
                <p>{{__('Please click one of the links above to proceed')}}</p>
            </div>
        </div>
    </section>
@endsection