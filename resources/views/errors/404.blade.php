@extends('layouts.app')

@section('content')
    <section class="container page">
        <div class="row">
            <div class="col">
                <h1>{{__('Whoops')}}...</h1>
                <p>{{__('Sorry, can\'t find the page requested (404).')}}</p>
                <p>{{__('Please click one of the links above to proceed.')}}</p>
            </div>
        </div>
    </section>
@endsection