@extends('layouts.app')

@section('content')
    <section class="container page">
        <div class="row">
            <div class="col">
                <h1>{{__('Forbidden')}}...</h1>
                <p>{{__('Sorry, you don\'t have permission to view the requested resource (403).')}}</p>
                <p>{{__('Please click one of the links above to proceed')}}</p>
            </div>
        </div>
    </section>
@endsection