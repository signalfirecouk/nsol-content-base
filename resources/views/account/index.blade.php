@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Account')])
@endsection


@section('content')
    <section class="container account">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
