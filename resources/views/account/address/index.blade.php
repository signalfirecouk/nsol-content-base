@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Addresses')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container addresses">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered">
                    <tr>
                        <th>{{__('Forename')}}</th>
                        <th>{{__('Surname')}}</th>
                        <th>{{__('Address1')}}</th>
                        <th>{{__('Town/City')}}</th>
                        <th>{{__('Postcode')}}</th>
                        <th></th>
                    </tr>
                    @forelse($addresses as $address)
                    <tr>
                        <td class="align-middle">{{$address->forename}}</td>
                        <td class="align-middle">{{$address->surname}}</td>
                        <td class="align-middle">{{$address->address1}}</td>
                        <td class="align-middle">{{$address->towncity}}</td>
                        <td class="align-middle">{{$address->postcode}}</td>
                        <td class="align-middle"><a href="{{route('account.addresses.edit', ['id' => $address->id])}}" class="btn btn-sm btn-primary">{{__('Edit')}}</a></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">{{__('No addresses found')}}</td>
                    </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </section>
@endsection