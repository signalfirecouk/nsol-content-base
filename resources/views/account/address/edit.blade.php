@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Edit Address')])
@endsection

@section('content')
    <section class="container address">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Address Details</div>
                    <div class="card-body">
                        <form action="{{route('account.addresses.update', ['id' => $address->id])}}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <label for="forename" class="col-md-3 col-form-label text-md-right">{{ __('Forename') }}</label>
                
                                <div class="col-md-9">
                                    <input id="forename" type="text" class="form-control @error('forename') is-invalid @enderror" name="forename" value="{{ old('forename', $address->forename) }}">
                
                                    @error('forename')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="surname" class="col-md-3 col-form-label text-md-right">{{ __('Surname') }}</label>
                
                                <div class="col-md-9">
                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname', $address->surname) }}">
                
                                    @error('surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="form-group row">
                                <label for="address-1" class="col-md-3 col-form-label text-md-right">{{ __('Address 1') }}</label>
                
                                <div class="col-md-9">
                                    <input id="address-1" type="text" class="form-control @error('address-1') is-invalid @enderror" name="address-1" value="{{ old('address-1', $address->address1) }}">
                
                                    @error('address-1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address-2" class="col-md-3 col-form-label text-md-right">{{ __('Address 2') }}</label>
                
                                <div class="col-md-9">
                                    <input id="address-2" type="text" class="form-control @error('address-2') is-invalid @enderror" name="address-2" value="{{ old('address-2', $address->address2) }}">
                
                                    @error('address-2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="form-group row">
                                <label for="address-3" class="col-md-3 col-form-label text-md-right">{{ __('Address 3') }}</label>
                
                                <div class="col-md-9">
                                    <input id="address-3" type="text" class="form-control @error('address-3') is-invalid @enderror" name="address-3" value="{{ old('address-3', $address->address3) }}">
                
                                    @error('address-3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address-4" class="col-md-3 col-form-label text-md-right">{{ __('Address 4') }}</label>
                
                                <div class="col-md-9">
                                    <input id="address-4" type="text" class="form-control @error('address-4') is-invalid @enderror" name="address-4" value="{{ old('address-4', $address->address4) }}">
                
                                    @error('address-4')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="towncity" class="col-md-3 col-form-label text-md-right">{{ __('Town / City') }}</label>
                
                                <div class="col-md-9">
                                    <input id="towncity" type="text" class="form-control @error('towncity') is-invalid @enderror" name="towncity" value="{{ old('towncity', $address->towncity) }}">
                
                                    @error('towncity')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="form-group row">
                                <label for="county" class="col-md-3 col-form-label text-md-right">{{ __('County') }}</label>
                
                                <div class="col-md-9">
                                    <input id="county" type="text" class="form-control @error('county') is-invalid @enderror" name="county" value="{{ old('county', $address->county) }}">
                
                                    @error('county')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="form-group row">
                                <label for="postcode" class="col-md-3 col-form-label text-md-right">{{ __('Postcode') }}</label>
                
                                <div class="col-md-7">
                                    <input id="postcode" type="text" class="form-control @error('postcode') is-invalid @enderror" name="postcode" value="{{ old('postcode', $address->postcode) }}">
                                    @error('postcode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-2">
                                    <button id="find-address" type="button" class="btn btn-primary btn-block find-address">Find Address</button>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="country" class="col-md-3 col-form-label text-md-right">{{ __('Country') }}</label>
                
                                <div class="col-md-9">
                                    <select id="country" class="form-control @error('country') is-invalid @enderror" name="country">
                                        @foreach($countries as $country)
                                            <option value="{{$country->name}}" {{old('country', $address->country) === $country->name ? 'selected': ''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                
                                    @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>  
                            <div class="form-group row">
                                <label for="telephone" class="col-md-3 col-form-label text-md-right">{{ __('Telephone') }}</label>
                
                                <div class="col-md-9">
                                    <input id="telephone" type="text" class="form-control @error('telephone') is-invalid @enderror" name="telephone" value="{{ old('telephone', $address->telephone) }}">
                
                                    @error('telephone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="mobile" class="col-md-3 col-form-label text-md-right">{{ __('Mobile') }}</label>
                
                                <div class="col-md-9">
                                    <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile', $address->mobile) }}">
                
                                    @error('mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update Address') }}
                                    </button>
                                </div>
                            </div> 
                            @include('partials.account.address.modal')
                        </form>                     
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection