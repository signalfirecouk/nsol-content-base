@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Shop')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12 col-md-8">
                @include('partials.ecommerce.mode.chooser')
            </div>
            <div class="col-12 col-md-4">
                @include('partials.ecommerce.sort.select')
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.ecommerce.categories.list')
            </div>
            <div class="col-12 col-md-9">
                @include('partials.ecommerce.products')
            </div>
        </div>
    </section>
@endsection