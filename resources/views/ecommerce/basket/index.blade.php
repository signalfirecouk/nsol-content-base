@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Basket')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered">
                    @include('partials.ecommerce.basket.table.header')
                    @forelse($items as $item)
                        @include('partials.ecommerce.basket.table.item.basket')
                        @include('partials.ecommerce.basket.table.footer.basket')
                    @empty
                        <tr>
                            <td colspan="5">{{__('No items in your basket')}}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </section>
@endsection