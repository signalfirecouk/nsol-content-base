@extends('layouts.app')

@section('title'){{$product->meta_title}}@endsection

@section('description'){{$product->meta_description}}@endsection

@section('banner')
    @include('partials.layout.banner', ['title' => $product->title])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12 col-md-8">

            </div>
            <div class="col-12 col-md-4">

            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12 col-md-6">
                @include('partials.bootstrap.carousel', ['pictures' => ProductHelper::pictures($product)])
            </div>
            <div class="col-12 col-md-6">
                @include('partials.ecommerce.basket.form')
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12">
                {{$product->description}}
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @include('partials.bootstrap.tabs', ['id' => 'product', 'tabs' => $product->tabs])
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script nonce="{{csp_nonce()}}">
        jQuery(document).ready(function(){
            singlePopulateQuantity(jQuery('#option'));
            singleSetPrice(jQuery('#option'));
        });
    </script>
@endsection