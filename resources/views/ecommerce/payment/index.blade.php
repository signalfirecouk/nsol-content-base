@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Payment')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered">
                    @include('partials.ecommerce.invoice.table.header')
                    @forelse($invoice->invoiceItems()->get() as $item)                               
                        @include('partials.ecommerce.invoice.table.item')
                        @include('partials.ecommerce.invoice.table.footer')
                    @empty
                        <tr>
                            <td colspan="4">{{__('No items to make payment')}}</td>
                        </tr>
                    @endforelse
                </table>
                @include('partials.ecommerce.payment.form')
                @include('partials.ecommerce.payment.modal')
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v3/" nonce="{{ csp_nonce() }}"></script>
    <script nonce="{{ csp_nonce() }}">
        jQuery(document).ready(function(){
            var stripe = Stripe("{{config('services.stripe.key')}}");
            var endpoint = "{{route('ecommerce.stripe.intent')}}";
            var redirect = "{{route('ecommerce.payment.redirect')}}";
            paymentStripeSetup(stripe, endpoint, redirect);
        });
    </script>
@endsection