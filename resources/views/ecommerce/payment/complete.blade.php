@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Payment Complete')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12">
                <p>Payment is completed with reference {{$id}}</p>
            </div>
        </div>
    </section>
@endsection