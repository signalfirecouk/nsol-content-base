@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Stores')])
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12">
                @forelse ($stores as $store)
                    <div class="card mb-3">
                        <div class="card-body">
                            <h2 class="card-title">{{$store->name}}</h2>
                            <p>{{$store->address1}}, {{$store->towncity}}, {{$store->county}}, {{$store->postcode}}</p>
                            <h3>Opening Hours</h3>
                            <div class="row mb-3">
                                @if (isset($store->hours['Monday']))
                                    <div class="col">
                                        <strong>Monday</strong><br/>
                                        {{$store->hours['Monday']}}
                                    </div>
                                @endif
                                @if (isset($store->hours['Tuesday']))
                                    <div class="col">
                                        <strong>Tuesday</strong><br/>
                                        {{$store->hours['Tuesday']}}
                                    </div>           
                                @endif      
                                @if (isset($store->hours['Wednesday']))
                                    <div class="col">
                                        <strong>Wednesday</strong><br/>
                                        {{$store->hours['Wednesday']}}
                                    </div>       
                                @endif                         
                                @if (isset($store->hours['Thursday']))
                                    <div class="col">
                                        <strong>Thursday</strong><br/>
                                        {{$store->hours['Thursday']}}
                                    </div>                                
                                @endif
                                @if (isset($store->hours['Friday']))
                                    <div class="col">
                                        <strong>Friday</strong><br/>
                                        {{$store->hours['Friday']}}
                                    </div>                                
                                @endif
                                @if (isset($store->hours['Saturday']))
                                    <div class="col">
                                        <strong>Saturday</strong><br/>
                                        {{$store->hours['Saturday']}}
                                    </div>                         
                                @endif      
                                @if (isset($store->hours['Sunday']))
                                    <div class="col">
                                        <strong>Sunday</strong><br/>
                                        {{$store->hours['Sunday']}}
                                    </div>                          
                                @endif      
                            </div>
                            <h3>Contact</h3>
                            <p class="mb-0"><strong>Email:</strong> {{$store->email}}<br/><strong>Telephone:</strong> {{$store->telephone}}</p>                            
                        </div>
                    </div>
                @empty
                
                @endforelse
            </div>
        </div>
    </section>
@endsection