@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Checkout')])
@endsection

@section('messages')
    @include('partials.bootstrap.alerts')
@endsection

@section('content')
    <section class="container ecommerce">
        <div class="row">
            <div class="col-12">
                <table class="table table-bordered">
                    @include('partials.ecommerce.basket.table.header')
                    @forelse($items as $item)
                        @include('partials.ecommerce.basket.table.item.checkout')
                        @include('partials.ecommerce.basket.table.footer.checkout')
                    @empty
                        <tr>
                            <td colspan="5">{{__('No items in your basket')}}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
        <form method="POST" action="{{route('ecommerce.checkout.store')}}">
            @csrf
            <div class="row mb-3">
                <div class="col-12">
                    <div class="card mb-3">
                        <div class="card-header">Payment Address</div>
                        <div class="card-body">
                            @include('partials.ecommerce.address.form', ['prefix' => 'payment'])
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">Delivery Address</div>
                        <div class="card-body">
                            @include('partials.ecommerce.address.form', ['prefix' => 'delivery'])
                        </div>
                    </div>         
                    <div id="collect" class="card mt-3" class="hidden">
                        <div class="card-header">Click and Collect Store</div>
                        <div class="card-body">
                            @include('partials.ecommerce.store.form')
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary mr-2">Payment</button>
                    <a href="{{route('ecommerce.basket')}}" class="btn btn-secondary">Return to Basket</a>
                </div>
            </div>   
            @include('partials.account.address.modal')
        </form>     
    </section>
@endsection

@section('scripts')
    <script nonce="{{ csp_nonce() }}">
        jQuery(document).ready(function(){
            checkoutToggleAddress('payment', jQuery('#payment-address'));
            checkoutToggleAddress('delivery', jQuery('#delivery-address'));
            checkoutToggleClickCollectStore();
        });
    </script>
@endsection