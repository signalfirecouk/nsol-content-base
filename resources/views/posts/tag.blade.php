@extends('layouts.app')

@section('title'){{$tag->meta_title}}@endsection

@section('description'){{$tag->meta_description}}@endsection

@section('banner')
    @include('partials.layout.banner', ['title' => __('Posts for') . ' ' . $tag->name])
@endsection

@section('content')
    <section class="container post-tag">
        <div class="row">
            <div class="col-12 col-md-9">
                @forelse ($posts as $post)
                    @include('partials.posts.teaser')
                @empty
                    <p>{{__('Sorry no posts found')}}</p>
                @endforelse
                {{ $posts->links() }}
            </div>
            <div class="col-12 col-md-3">
                @include('partials.posts.search')
                @include('partials.posts.tags')
            </div>
        </div>
    </section>
@endsection