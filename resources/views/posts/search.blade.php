@extends('layouts.app')

@section('banner')
    @include('partials.layout.banner', ['title' => __('Search Results')])
@endsection

@section('content')
    <section class="container post-index">
        <div class="row">
            <div class="col-12 col-md-9">
                @forelse ($posts as $post)
                    @include('partials.posts.teaser')
                @empty
                    <p>{{__('Sorry, no posts match the search criteria provided at this time')}}</p>
                @endforelse    
                {{ $posts->links() }}
            </div>
            <div class="col-12 col-md-3">
                @include('partials.posts.search')
                @include('partials.posts.tags')
            </div>
        </div>
    </section>
@endsection