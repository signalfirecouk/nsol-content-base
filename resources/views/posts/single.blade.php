@extends('layouts.app')

@section('title'){{$post->meta_title}}@endsection

@section('description'){{$post->meta_description}}@endsection

@section('banner')
    @include('partials.layout.banner', ['title' => $post->title])
@endsection

@section('content')
    <section class="container post-single">
        <div class="row">
            <div class="col-12 col-md-9">
                <article>
                    @include('partials.posts.picture')            
                    @include('partials.posts.meta')
                    <p>{!!ContentHelper::nl2p($post->content)!!}</p>
                    @include('partials.posts.gallery')
                    @include('partials.posts.files')
                    @include('partials.bootstrap.carousel', ['pictures' => $post->pictures()->get()])
                </article>
            </div>
            <div class="col-12 col-md-3">
                @include('partials.posts.search')
                @include('partials.posts.tags')
            </div>
        </div>
    </section>
@endsection