@extends('layouts.app')

@section('title'){{$user->forename}} {{$user->surname}} @endsection

@section('description'){{__('Posts written by')}} {{$user->forename}} {{$user->surname}} @endsection

@section('banner')
    @include('partials.layout.banner', ['title' => __('Posts by') . ' ' . $user->forename . ' ' . $user->surname])
@endsection

@section('content')
    <section class="container post-author">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                @forelse ($posts as $post)
                    @include('partials.posts.teaser')
                @empty
                    <p>{{__('Sorry no posts found')}}</p>
                @endforelse
                {{ $posts->links() }}
            </div>
            <div class="col-sm-12 col-md-3">
                @include('partials.posts.search')
                @include('partials.posts.tags')
            </div>
        </div>
    </section>
@endsection