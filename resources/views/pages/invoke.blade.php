@extends('layouts.app')

@section('title'){{$page->meta_title}}@endsection

@section('description'){{$page->meta_description}}@endsection

@section('content')
    <section class="container page">
        <div class="row">
            <div class="col">
                <article>
                    <h1>{{$page->title}}</h1>
                    {!!ContentHelper::nl2p($page->content)!!}
                </article>
            </div>
        </div>
    </section>
@endsection