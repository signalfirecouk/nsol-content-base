@extends('layouts.app')

@section('content')
    <section class="container page">
        <div class="row">
            <div class="col">
                <h1>{{__('Contact Sent')}}</h1>
            </div>
        </div>
    </section>
@endsection