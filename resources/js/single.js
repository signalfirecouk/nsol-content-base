const singlePopulateQuantity = ($el) => {
    const stock = parseInt($el.find(':selected').data('stock'));
    const $quantity = jQuery('#quantity');
    if (stock > 10) {
        stock = 10;
    }
    $quantity.empty();
    for(var i = 1; i <= stock; i++){
        $quantity.append(jQuery('<option/>').val(i).text(i));
    }
}

const singleSetPrice = ($el) => {
    const price = $el.find(':selected').data('price');
    jQuery('#price').text(price);
}

const singleChangeOptionPictures = ($el) => {
    const id = $el.find(':selected').val(); 
    jQuery.getJSON(`/api/shop/options/${id}/pictures`, function(result){
        if (result.data.length === 0) return;
        const $carousel = jQuery('#gallery-carousel');
        const $indicators = $carousel.find('.carousel-indicators');
        const $inner = $carousel.find('.carousel-inner');
        $indicators.empty();
        $inner.empty();
        jQuery.each(result.data, function(index, picture){
            $indicators
                .append(jQuery('<li/>')
                    .attr('data-target', '#gallery-carousel')
                    .attr('data-slide-to', index)
                    .attr('class', index === 0 ? 'active' : ''));
            $inner
                .append(jQuery('<div/>')
                    .attr('class', index === 0 ? 'carousel-item active' : 'carousel-item')
                    .append(jQuery('<img/>')
                        .attr('src', `/image/730/380/${picture.filename}`)
                        .attr('class', 'd-block w-100')
                        .attr('alt', picture.caption)
                    )
                );
        })
    });
}

jQuery('#option').on('change', function(){
    singlePopulateQuantity(jQuery(this));
    singleSetPrice(jQuery(this));
    singleChangeOptionPictures(jQuery(this));
});


window.singlePopulateQuantity = singlePopulateQuantity;
window.singleSetPrice = singleSetPrice;
window.singleChangeOptionPictures = singleChangeOptionPictures;