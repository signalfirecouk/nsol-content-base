/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// Util functions
require('./utils');

// Checkout functions
require('./checkout');

// Address functions
require('./address');

// Payment functions
require('./payment');

// Single Product functions
require('./single');