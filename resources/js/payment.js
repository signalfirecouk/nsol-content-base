// Functions
const paymentShowError = (message) => {
    const $modal = jQuery('#payment-modal');
    const $error = jQuery('#payment-modal-error');
    $error.text(message);
    $modal.modal();
}

const paymentRedirectSuccess = (redirect, id) => {
    window.location.href = redirect + '?pid=' + id;
}

const paymentStripeSetup = (stripe, endpoint, redirect) => {
    jQuery('#submit').attr('disabled', true);

    jQuery.getJSON(endpoint)
        .done(function(data){
            const elements = stripe.elements();
            const style = {
                base: {
                    color: "#32325d",
                    fontFamily: 'Arial, sans-serif',
                    fontSmoothing: "antialiased",
                    fontSize: "16px",
                    "::placeholder": {
                        color: "#32325d"
                    }
                },
                invalid: {
                    fontFamily: 'Arial, sans-serif',
                    color: "#fa755a",
                    iconColor: "#fa755a"
                }
            };

            const card = elements.create("card", {style:style});
            card.mount("#card-element");
            card.on('change', function(event){
                jQuery('#submit').attr('disabled', event.empty);
                jQuery('#card-error').textContent = event.error ? event.error.message : '';
            });

            jQuery('#payment-form').submit(function(event){
                event.preventDefault();
                stripe
                    .confirmCardPayment(data.secret, {
                        payment_method: {
                            card: card
                        }
                    })
                    .then(function(result) {
                        if (result.error) {
                            // Show error to your customer
                            paymentShowError(result.error.message);
                        } else {
                            // The payment succeeded!
                            paymentRedirectSuccess(redirect, result.paymentIntent.id);
                        }
                    });   
            });
        })
        .fail(function( jqxhr, textStatus, error ){
            paymentShowError('Payment failed, unable to get payment intent');
        });
}

// Global
window.paymentStripeSetup = paymentStripeSetup;