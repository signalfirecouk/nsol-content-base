let addresses = null;

const checkoutToggleAddress = (prefix, $el) => {
    const id = getElementId(prefix, 'fields');
    if ($el.val() === '00000000-0000-0000-0000-000000000000') {
        jQuery(id).removeClass('hidden');
    }else{
        jQuery(id).addClass('hidden');
    }            
}

const checkoutToggleClickCollectStore = () => {
    const $el = jQuery('#delivery-address');
    if ($el.val() === '00000000-0000-0000-0000-000000000002') {
        jQuery('#collect').removeClass('hidden');
    }else{
        jQuery('#collect').addClass('hidden');
    }
}

// JQuery Events
jQuery('.address-selector').on('change', function(){
    const $el = jQuery(this);
    const prefix = $el.data('prefix');
    checkoutToggleAddress(prefix, $el);
});

jQuery('#delivery-address').on('change', function(){
    checkoutToggleClickCollectStore();
});

// jQuery('.find-address').on('click touchstart', function(){
//     const prefix = jQuery(this).data('prefix');
//     checkoutFindAddress(prefix);
// });

// jQuery('#address-modal-use').on('click touchstart', function(){
//     const prefix = jQuery(this).data('prefix');
//     checkoutUseAddress(prefix);
// });

// Global
window.checkoutToggleAddress = checkoutToggleAddress;
window.checkoutToggleClickCollectStore = checkoutToggleClickCollectStore;