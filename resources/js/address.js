let addresses = null;

// Functions
// const checkoutGetElementId = (prefix, suffix) => {
//     return prefix ? '#' + prefix + '-' + suffix : '#' + suffix;
// }

const checkoutFindAddress = (prefix) => {
    const id = getElementId(prefix, 'postcode');
    const postcode = jQuery(id).val();
    const regex = RegExp(/^([A-Za-z][A-Ha-hJ-Yj-y]?[0-9][A-Za-z0-9]? ?[0-9][A-Za-z]{2}|[Gg][Ii][Rr] ?0[Aa]{2})$/);
    const $postcode = jQuery(id);
    const $modal = jQuery('#address-modal');
    const $error = jQuery('#address-modal-error');
    const $button = jQuery('#address-modal-use');
    const $results = jQuery('#address-modal-results');
    
    if (!regex.test(postcode)){
        $postcode.addClass('is-invalid');
        return;
    }else{
        $postcode.removeClass('is-invalid');
        if (prefix){
            $button.data('prefix', prefix);
        }
        $modal.modal();
        $results.empty();
        $results.removeClass('hidden');
        $button.removeClass('hidden');
        $error.addClass('hidden');                
        jQuery.getJSON('/account/address/lookup/' + postcode, function(data){
            addresses = data.addresses;
            jQuery.each(data.addresses, function(index, item){
                $results.append(jQuery('<option/>').val(index).text(item.formatted_address));
            });
        })
        .fail(function(){
            $results.addClass('hidden');
            $button.addClass('hidden');
            $error.removeClass('hidden');
        });     
    }
}

const checkoutUseAddress = (prefix) => {
    const $results = jQuery('#address-modal-results');
    const $modal = jQuery('#address-modal');
    const $address1 = jQuery(getElementId(prefix,'address-1'));
    const $address2 = jQuery(getElementId(prefix,'address-2'));
    const $towncity = jQuery(getElementId(prefix,'towncity'));
    const $county = jQuery(getElementId(prefix,'county'));
    const $country = jQuery(getElementId(prefix,'country'));
    if ($results.val()){
        const address = addresses[$results.val()];
        $address1.val(address.line_1);
        $address2.val(address.line_2);
        $towncity.val(address.town_or_city);
        $county.val(address.county);
        $country.val(address.country);
    }
    $modal.modal('hide')    
}

jQuery('.find-address').on('click touchstart', function(){
    const prefix = jQuery(this).data('prefix') || null;
    checkoutFindAddress(prefix);
});

jQuery('#address-modal-use').on('click touchstart', function(){
    const prefix = jQuery(this).data('prefix') || null;
    checkoutUseAddress(prefix);
});
