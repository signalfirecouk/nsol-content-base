// Functions
const getElementId = (prefix, suffix) => {
    return prefix ? '#' + prefix + '-' + suffix : '#' + suffix;
}

window.getElementId = getElementId;