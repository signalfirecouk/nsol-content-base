<?php

namespace App\Http\Middleware;

use Closure;

use FeatureHelper;

class CheckSiteEcommerceEnabled
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!FeatureHelper::enabled('ecommerce', config('site.features'))) {
            return abort(403);
        }

        return $next($request);
    }
}
