<?php

namespace App\Http\Requests\Ecommerce;

use Illuminate\Foundation\Http\FormRequest;

class StoreCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment-address' => 'required|uuid',
            'payment-forename' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:30',
            'payment-surname' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:30',
            'payment-address-1' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'payment-address-2' => 'nullable|string|max:50',
            'payment-address-3' => 'nullable|string|max:50',
            'payment-address-4' => 'nullable|string|max:50',
            'payment-towncity' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'payment-county' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'payment-postcode' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:10',
            'payment-country' => 'required_if:payment-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'payment-telephone' => 'nullable|string|max:30',
            'payment-mobile' => 'nullable|string|max:30',
            
            'delivery-address' => 'required|uuid',
            'delivery-forename' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:30',
            'delivery-surname' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:30',
            'delivery-address-1' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'delivery-address-2' => 'nullable|string|max:50',
            'delivery-address-3' => 'nullable|string|max:50',
            'delivery-address-4' => 'nullable|string|max:50',
            'delivery-towncity' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'delivery-county' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'delivery-postcode' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:10',
            'delivery-country' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000000|nullable|string|max:50',
            'delivery-telephone' => 'nullable|string|max:30',
            'delivery-mobile' => 'nullable|string|max:30',

            'store' => 'required_if:delivery-address,00000000-0000-0000-0000-000000000002|nullable|uuid'
        ];
    }

    public function messages()
    {
        return [
            'payment-forename.required_if' => 'Forename field is required',
            'payment-surname.required_if' => 'Surname field is required',
            'payment-address-1.required_if' => 'Address 1 field is required',
            'payment-towncity.required_if' => 'Town / City field is required',
            'payment-county.required_if' => 'County field is required',
            'payment-postcode.required_if' => 'Postcode field is required',
            'payment-country.required_if' => 'Country field is required',

            'delivery-forename.required_if' => 'Forename field is required',
            'delivery-surname.required_if' => 'Surname field is required',
            'delivery-address-1.required_if' => 'Address 1 field is required',
            'delivery-towncity.required_if' => 'Town / City field is required',
            'delivery-county.required_if' => 'County field is required',
            'delivery-postcode.required_if' => 'Postcode field is required',
            'delivery-country.required_if' => 'Country field is required',
            
            'store.required_if' => 'Store for collection required',
        ];
    }
}
