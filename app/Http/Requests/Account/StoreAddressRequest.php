<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forename' => 'required|string|max:30',
            'surname' => 'required|string|max:30',
            'address-1' => 'required|string|max:50',
            'address-2' => 'nullable|string|max:50',
            'address-3' => 'nullable|string|max:50',
            'address-4' => 'nullable|string|max:50',
            'towncity' => 'required|string|max:50',
            'county' => 'required|string|max:50',
            'postcode' => 'required|string|max:10',
            'country' => 'required|string|max:50',
            'telephone' => 'nullable|string|max:30',
            'mobile' => 'nullable|string|max:30',
        ];
    }

    public function messages()
    {
        return [
            'forename.required' => 'Forename field is required',
            'surname.required' => 'Surname field is required',
            'address-1.required' => 'Address 1 field is required',
            'towncity.required' => 'Town / City field is required',
            'county.required' => 'County field is required',
            'postcode.required' => 'Postcode field is required',
            'country.required' => 'Country field is required'
        ];
    }
}
