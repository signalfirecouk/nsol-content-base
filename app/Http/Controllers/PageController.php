<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;

class PageController extends Controller
{
    public function __invoke($slug)
    {
        $page = Page::live()
            ->where('slug', $slug)
            ->firstOrFail();

        return view('pages.invoke', ['page' => $page]);
    }
}
