<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;

use App\Http\Requests\StoreContactRequest;
use App\Mail\ContactReceivedMail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function store(StoreContactRequest $request)
    {
        $validated = $request->validated();

        Mail::to(config('mail.to.address'))
            ->send(new ContactReceivedMail($validated));

        return redirect()
            ->route('contact.thanks');
    }

    public function thanks()
    {
        return view('contact.thanks');
    }
}
