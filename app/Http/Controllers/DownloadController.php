<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\File;

use Storage;

class DownloadController extends Controller
{
    public function __invoke($id)
    {
        $file = File::find($id)
            ->firstOrFail();

        return Storage::disk('public')
            ->download($file->filename);
    }
}
