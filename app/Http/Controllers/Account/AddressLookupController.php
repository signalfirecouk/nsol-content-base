<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Str;

use GuzzleHttp\Client;

class AddressLookupController extends Controller
{
    private function endpoint($postcode)
    {
        return sprintf(
            'https://api.getAddress.io/find/%s?api-key=%s&expand=true',
            $postcode,
            config('services.getaddress.key')
        );
    }

    private function cacheKey($postcode)
    {
        return Str::slug(strtolower($postcode));
    }

    public function __invoke($postcode)
    {
        $key = $this->cacheKey($postcode);

        if (Cache::has($key)) {
            return response()->json(json_decode(Cache::get($key)));
        }

        try {
            $client = new Client;
            $response = $client->get($this->endpoint($postcode));
            if ($response->getStatusCode() === 200) {
                $contents = $response->getBody()->getContents();
                if ($contents) {
                    Cache::put($key, $contents, 3600);
                    return response()->json(json_decode($contents));
                }
            }
        } catch (Exception $ex) {
            Log::error($ex);
        }

        return response()->json([], 404);
    }
}
