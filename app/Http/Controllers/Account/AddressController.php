<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests\Account\StoreAddressRequest;

use App\Models\Address;
use App\Models\DeliveryCountry;

class AddressController extends Controller
{
    public function index(Request $request)
    {
        $addresses = $request
            ->user()
            ->addresses()
            ->get();

        return view('account.address.index', [
            'addresses' => $addresses,
        ]);
    }

    public function create(Request $request)
    {
        $countries = DeliveryCountry::orderBy('sort', 'ASC')->get();
        return view('account.address.add', [
            'countries' => $countries
        ]);
    }

    public function edit($id, Request $request)
    {
        $address = Address::where([
            ['id', '=', $id],
            ['user_id', '=', $request->user()->id]
        ])->firstOrFail();

        $countries = DeliveryCountry::orderBy('sort', 'ASC')->get();

        return view('account.address.edit', [
            'address' => $address,
            'countries' => $countries
        ]);
    }

    public function store(StoreAddressRequest $request)
    {
        $validated = $request->validated();

        $address = new Address;

        $address->forename = $validated['forename'];
        $address->surname = $validated['surname'];
        $address->address1 = $validated['address-1'];
        $address->address2 = $validated['address-2'];
        $address->address3 = $validated['address-3'];
        $address->address4 = $validated['address-4'];
        $address->towncity = $validated['towncity'];
        $address->county = $validated['county'];
        $address->postcode = $validated['postcode'];
        $address->country = $validated['country'];
        $address->telephone = $validated['telephone'];
        $address->mobile = $validated['mobile'];

        $request->user()->addresses()->save($address);

        return redirect()->route('account.addresses')->with('status', __('Address has been created'));
    }

    public function update($id, StoreAddressRequest $request)
    {
        $validated = $request->validated();

        $address = Address::where([
            ['id', '=', $id],
            ['user_id', '=', $request->user()->id]
        ])->firstOrFail();

        $address->forename = $validated['forename'];
        $address->surname = $validated['surname'];
        $address->address1 = $validated['address-1'];
        $address->address2 = $validated['address-2'];
        $address->address3 = $validated['address-3'];
        $address->address4 = $validated['address-4'];
        $address->towncity = $validated['towncity'];
        $address->county = $validated['county'];
        $address->postcode = $validated['postcode'];
        $address->country = $validated['country'];
        $address->telephone = $validated['telephone'];
        $address->mobile = $validated['mobile'];

        $address->save();

        return redirect()->route('account.addresses')->with('status', __('Address has been updated'));
    }
}
