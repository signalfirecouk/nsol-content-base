<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use PDF;

class InvoicePDFController extends Controller
{
    public function __invoke($id)
    {
        // Get invoice by id and user to ensure owned

        $pdf = PDF::loadView('ecommerce.invoice.pdf', []);

        return $pdf->download("{$id}.pdf");
    }
}
