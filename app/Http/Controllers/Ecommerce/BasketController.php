<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Traits\HandlesBasket;

use App\Models\BasketItem;

class BasketController extends Controller
{
    use HandlesBasket;

    public function index(Request $request)
    {
        $identifier = $request->cookie('identifier');
        $items = BasketItem::where('identifier', $identifier)->get();
        return view('ecommerce.basket.index', ['items' => $items]);
    }
}
