<?php

namespace App\Http\Controllers\Ecommerce;

use UnexpectedValueException;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Log;

use Stripe\Stripe;
use Stripe\PaymentIntent;
use Stripe\Event;

use App\Models\Invoice;
use App\Models\InvoiceNote;
use App\Models\Enums\InvoiceNoteVisibility;
use App\Models\Enums\InvoiceStatus;

use App\Helpers\BasketHelper;

use App\Events\InvoicePaidEvent;

class StripeController extends Controller
{
    public function intent(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        $identifier = $request->cookie('identifier');

        if (!$identifier) {
            return response()
                ->json(['error' => 'Identifier not found'], 404);
        }

        $invoice = Invoice::where('identifier', $identifier)->firstOrFail();

        try {
            $intent = PaymentIntent::create([
                'metadata' => [
                    'invoice_id' => $invoice->id,
                ],
                'amount' => BasketHelper::valueAsPence($invoice->total),
                'currency' => config('services.stripe.currency'),
            ]);

            
            return response()->json(['secret' => $intent->client_secret]);
        } catch (Exception $ex) {
            Log::error($ex);
            return response()->json(['error' => $ex->message]);
        }
    }

    public function webhook(Request $request)
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $event = Event::constructFrom($request->input());
        } catch (UnexpectedValueException $ex) {
            Log::error($ex);
            return response()
                ->json(['message' => 'Unexpected value exception'], 400);
        }

        switch ($event->type) {
            case 'payment_intent.succeeded':
                $intent = $event->data->object;

                $invoice = Invoice::where('id', $intent->metadata->invoice_id)->first();

                if (!$invoice) {
                    return response()
                        ->json(['message' => 'Invoice not found'], 400);
                }

                $invoice->stripe_payment_id = $intent->id;
                $invoice->status = InvoiceStatus::PAID;
                $invoice->invoiceNotes()->create([
                    'visiblity' => InvoiceNoteVisibility::STAFF,
                    'content' => json_encode($intent)
                ]);

                $invoice->save();

                event(new InvoicePaidEvent($invoice));

                break;
            default:
                return response()
                    ->json(['message' => 'Unexpected event type'], 400);
        }

        return response()
            ->json(['message' => 'OK'], 200);
    }
}
