<?php

namespace App\Http\Controllers\Ecommerce\API;

use App\Http\Resources\Option as OptionResource;

use App\Models\Option;

class ProductController
{
    public function options($id)
    {
        $options = Option::where('product_id', $id)
            ->live()
            ->hasStock()
            ->get();

        return OptionResource::collection($options);
    }
}
