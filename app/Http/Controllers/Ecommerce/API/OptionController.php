<?php

namespace App\Http\Controllers\Ecommerce\API;

use App\Models\Option;
use App\Http\Resources\Picture as PictureResource;

class OptionController
{
    public function pictures($id)
    {
        $option = Option::where('id', $id)
            ->live()
            ->hasStock()
            ->first();

        return PictureResource::collection($option->pictures()->get());
    }
}
