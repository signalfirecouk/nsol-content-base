<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Cookie;

use App\Models\Invoice;
use App\Helpers\BasketHelper;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $identifier = $request->cookie('identifier');

        $invoice = Invoice::where('identifier', $identifier)
            ->firstOrFail();

        return view('ecommerce.payment.index', ['invoice' => $invoice]);
    }

    public function redirect(Request $request)
    {
        if (!$request->has('pid') || !preg_match('/^pi_[a-z0-9]{24}$/i', $request->query('pid'))) {
            return redirect()
                ->route('ecommerce')
                ->with('warning', __('Payment ID not specified or invalid'));
        }

        $identifier = $request->cookie('identifier');

        if (!$identifier) {
            return redirect()
                ->route('ecommerce')
                ->with('warning', __('Unable to get cookie identifier'));
        }

        BasketHelper::clear($identifier);

        return
            redirect()
                ->route('ecommerce.payment.complete', ['pid' => $request->query('pid')])
                ->withCookie(Cookie::forget('identifier'));
    }

    public function complete(Request $request)
    {
        if (!$request->has('pid') || !preg_match('/^pi_[a-z0-9]{24}$/i', $request->query('pid'))) {
            return redirect()
                ->route('ecommerce')
                ->with('warning', __('Payment ID not specified or invalid'));
        }

        return view('ecommerce.payment.complete', ['id' => $request->input('pid')]);
    }
}
