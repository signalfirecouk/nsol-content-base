<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\Category;

class HomeController extends Controller
{
    protected $pagesize = 9;

    public function index()
    {
        $products = Product::live()
            ->paginate($this->pagesize);
        $categories = Category::live()
            ->whereNull('parent_category_id')
            ->get();

        return view('ecommerce.index', [
            'products' => $products,
            'categories' => $categories
        ]);
    }

    public function category($slug)
    {
        $category = Category::live()
            ->where('slug', $slug)
            ->firstOrFail();
        $products = $category
            ->products()
            ->live()
            ->paginate($this->pagesize);
        $categories = Category::live()
            ->whereNull('parent_category_id')
            ->get();

        return view('ecommerce.category', [
            'products' => $products,
            'category' => $category,
            'categories' => $categories
        ]);
    }

    public function single($slug)
    {
        $product = Product::live()
            ->where('slug', $slug)
            ->firstOrFail();

        return view('ecommerce.single', ['product' => $product]);
    }
}
