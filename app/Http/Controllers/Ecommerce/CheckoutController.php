<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Traits\HandlesBasket;

use App\Models\BasketItem;
use App\Models\Address;
use App\Models\Invoice;
use App\Models\DeliveryCountry;
use App\Models\Store;

use App\Http\Requests\Ecommerce\StoreCheckoutRequest;

use App\Helpers\BasketHelper;

class CheckoutController extends Controller
{
    use HandlesBasket;

    private function getOrCreateAddress($request, $prefix, $data)
    {
        $address = Address::where('id', $data[$prefix . '-address'])->first();

        if (!$address) {
            $address = Address::create([
                'user_id' => $request->user()->id,
                'forename' => $data[$prefix . '-forename'],
                'surname' => $data[$prefix . '-surname'],
                'address1' => $data[$prefix . '-address-1'],
                'address2' => $data[$prefix . '-address-2'] ?: null,
                'address3' => $data[$prefix . '-address-3'] ?: null,
                'address4' => $data[$prefix . '-address-4'] ?: null,
                'towncity' => $data[$prefix . '-towncity'],
                'county' => $data[$prefix . '-county'],
                'postcode' => $data[$prefix . '-postcode'],
                'country' => $data[$prefix . '-country'],
                'telephone' => $data[$prefix . '-telephone'] ?: null,
                'mobile' => $data[$prefix . '-mobile'] ?: null,
            ]);
        }

        return $address;
    }

    private function deleteInvoiceAndItems($invoice)
    {
        $invoice->invoiceItems()->delete();
        return $invoice->delete();
    }

    private function createInvoice(
        $request,
        $identifier,
        $delivery_address,
        $payment_address,
        $store,
        $items
    ) {
        $invoice = Invoice::where('identifier', $identifier)->first();

        if ($invoice) {
            $this->deleteInvoiceAndItems($invoice);
        }

        $subtotal = BasketHelper::total($items); // Total of items based on price * quantity

        if ($delivery_address) {
            $delivery = BasketHelper::delivery($items, $delivery_address);
            if ($delivery === false) {
                return false;
            }
        } else {
            $delivery = 0;
        }

        // Delivery tax??

        $tax = BasketHelper::taxes($items); // Based on tax rate per item
        $extras = $delivery + $tax; // delivery fee + tax due
        $total = $subtotal + $extras;

        //ToDo Total
        return Invoice::create([
            'user_id' => $request->user()->id,
            'delivery_address_id' => $delivery_address
                ? $delivery_address->id
                : null,
            'store_id' => $store ? $store->id : null,
            'payment_address_id' => $payment_address->id,
            'identifier' => $identifier,
            'subtotal' => $subtotal,
            'delivery' => $delivery,
            'tax' => $tax,
            'total' => $total,
            'status' => 1,
        ]);
    }

    private function getDeliveryAddress($request, $validated, $payment)
    {
        switch ($validated['delivery-address']) {
            case '00000000-0000-0000-0000-000000000001':
                return $payment;
            case '00000000-0000-0000-0000-000000000002':
                return null;
            default:
                return $this->getOrCreateAddress(
                    $request,
                    'delivery',
                    $validated
                );
        }
    }

    private function getCollectionStore($validated)
    {
        if (!$validated['store']) {
            return null;
        }

        $store = Store::where('id', $validated['store'])->first();

        return $store;
    }

    public function index(Request $request)
    {
        $identifier = $request->cookie('identifier');

        $items = BasketItem::where('identifier', $identifier)->get();

        if ($items->count() === 0) {
            return redirect(route('ecommerce'))->with(
                'warning',
                __('Unable to checkout, no items in basket')
            );
        }

        $stores = Store::live()->get();

        $countries = DeliveryCountry::orderBy('sort', 'ASC')->get();

        return view('ecommerce.checkout.index', [
            'items' => $items,
            'countries' => $countries,
            'stores' => $stores,
        ]);
    }

    public function store(StoreCheckoutRequest $request)
    {
        $identifier = $request->cookie('identifier');

        $items = BasketItem::where('identifier', $identifier)->get();

        if ($items->count() === 0) {
            return redirect()
                ->back()
                ->with('warning', __('No items in basket to checkout'));
        }

        $validated = $request->validated();

        $payment = $this->getOrCreateAddress($request, 'payment', $validated);

        $delivery = $this->getDeliveryAddress($request, $validated, $payment);

        $store = $this->getCollectionStore($validated);

        $invoice = $this->createInvoice(
            $request,
            $identifier,
            $delivery,
            $payment,
            $store,
            $items
        );

        if (!$invoice) {
            return redirect()
                ->back()
                ->with(
                    'warning',
                    __('Problem with creating invoice, unable to checkout')
                );
        }

        // Iterate basket items and add to order
        foreach ($items as $item) {
            $invoice->invoiceItems()->create([
                'product_id' => $item->product_id,
                'option_id' => $item->option_id,
                'quantity' => $item->quantity,
                'price' => BasketHelper::price($item),
            ]);
        }

        return redirect()->route('ecommerce.payment');
    }
}
