<?php

namespace App\Http\Controllers\Ecommerce;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Store;

class StoreController extends Controller
{
    public function index()
    {
        $stores = Store::live()->get();

        return view('ecommerce.stores.index', ['stores' => $stores]);
    }
}
