<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Tag;
use App\Models\User;

class PostController extends Controller
{
    protected $pagesize = 10;

    public function index()
    {
        $posts = Post::live()
            ->orderBy('created_at')
            ->paginate($this->pagesize);
        
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    public function single($slug)
    {
        $post = Post::live()
            ->where('slug', $slug)
            ->firstOrFail();

        return view('posts.single', ['post' => $post]);
    }

    public function tag($slug)
    {
        $tag = Tag::live()
            ->where('slug', $slug)
            ->firstOrFail();
        
        $posts = $tag
            ->posts()
            ->live()
            ->paginate($this->pagesize);

        return view('posts.tag', [
            'tag' => $tag,
            'posts' =>  $posts
        ]);
    }

    public function search(Request $request)
    {
        $keywords = $request->query('keywords');

        $posts = Post::live()
            ->where('title', 'like', "%{$keywords}%")
            ->orWhere('content', 'like', "%{$keywords}%")
            ->orderBy('created_at')
            ->paginate($this->pagesize);

        return view('posts.search', [
            'posts' => $posts,
            'keywords' => $keywords
        ]);
    }

    public function author($slug)
    {
        $user = User::where('nickname', $slug)->firstOrFail();

        return view('posts.author', [
            'posts' => $user->posts()->live()->paginate(10),
            'user' => $user,
        ]);
    }
}
