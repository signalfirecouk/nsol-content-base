<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;
use Image;

class ImageController extends Controller
{
    public function __invoke($width, $height, $filename)
    {
        if (Storage::disk('public')->exists($filename)) {
            $img = Image::make(Storage::disk('public')->get($filename));
        } else {
            $img = Image::canvas(500, 300, '#f1f1f1');
            $img->text('Awaiting Picture', 250, 150, function ($font) {
                $font->file(Storage::disk('local')->path('fonts/placeholder.ttf'));
                $font->size(36);
                $font->align('center');
                $font->valign('middle');
                $font->color('#cccccc');
            });
        }
        
        $img->fit($width, $height);

        return $img->response();
    }
}
