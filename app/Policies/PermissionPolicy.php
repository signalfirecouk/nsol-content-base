<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use Spatie\Permission\Models\Permission;

use App\Models\User;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any permissions.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can view a specific permission
     *
     * @param  \App\Models\User                     $user
     * @param  \Spatie\Permission\Models\Permission $permission
     * @return mixed
     */
    public function view(User $user, Permission $permission)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can create a permission
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can update a specific permission
     *
     * @param  \App\Models\User                     $user
     * @param  \Spatie\Permission\Models\Permission $permission
     * @return mixed
     */
    public function update(User $user, Permission $permission)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can delete a specific permission
     *
     * @param  \App\Models\User                     $user
     * @param  \Spatie\Permission\Models\Permission $permission
     * @return mixed
     */
    public function delete(User $user, Permission $permission)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can restore a specific permission
     *
     * @param  \App\Models\User                     $user
     * @param  \Spatie\Permission\Models\Permission $role
     * @return mixed
     */
    public function restore(User $user, Permission $permission)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can forceDelete a specific permission
     *
     * @param  \App\Models\User                     $user
     * @param  \Spatie\Permission\Models\Permission $permission
     * @return mixed
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return $user->hasRole('Administrator');
    }
}
