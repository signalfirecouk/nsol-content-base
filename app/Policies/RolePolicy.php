<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use Spatie\Permission\Models\Role;

use App\Models\User;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any roles.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can view a specific role
     *
     * @param  \App\Models\User               $user
     * @param  \Spatie\Permission\Models\Role $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can create a role
     *
     * @param  \App\Models\User               $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can update a specific role
     *
     * @param  \App\Models\User               $user
     * @param  \Spatie\Permission\Models\Role $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can delete a specific role
     *
     * @param  \App\Models\User               $user
     * @param  \Spatie\Permission\Models\Role $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can restore a specific role
     *
     * @param  \App\Models\User               $user
     * @param  \Spatie\Permission\Models\Role $role
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
        return $user->hasRole('Administrator');
    }

    /**
     * Determine whether the user can forceDelete a specific role
     *
     * @param  \App\Models\User               $user
     * @param  \Spatie\Permission\Models\Role $role
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        return $user->hasRole('Administrator');
    }
}
