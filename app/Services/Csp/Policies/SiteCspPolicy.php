<?php

namespace App\Services\Csp\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Basic;

class SiteCspPolicy extends Basic
{
    public function configure()
    {
        parent::configure();
        
        $this
            ->addDirective(Directive::SCRIPT, 'kit.fontawesome.com')
            ->addDirective(Directive::FONT, 'fonts.gstatic.com')
            ->addDirective(Directive::FONT, 'kit-free.fontawesome.com')
            ->addDirective(Directive::STYLE, 'fonts.googleapis.com')
            ->addDirective(Directive::STYLE, 'kit-free.fontawesome.com')
            ->addDirective(Directive::IMG, 'data:')
            ->addDirective(Directive::IMG, 'secure.gravatar.com')
            ->addDirective(Directive::FRAME, 'js.stripe.com')
            ->addNonceForDirective(Directive::SCRIPT)
            ->addNonceForDirective(Directive::STYLE);
        $this->reportOnly();
    }
}
