<?php

namespace App\Classes;

use OptimistDigital\MenuBuilder\Classes\MenuLinkable;

use App\Models\Page;

class PageMenuLinkable extends MenuLinkable
{
    public static function getIdentifier(): string
    {
        return 'page';
    }
    
    public static function getName(): string
    {
        return 'Page Link';
    }

    public static function getOptions($locale): array
    {
        return Page::live()->pluck('title', 'id')->toArray();
    }
    
    public static function getDisplayValue($value = null, array $parameters = null)
    {
        $page = Page::find($value);
        return $page->title;
    }

    public static function getValue($value = null, array $parameters = null)
    {
        $page = Page::find($value);
        return route('page', ['slug' => $page->slug]);
    }
}
