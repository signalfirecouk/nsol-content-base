<?php

namespace App\Classes;

use OptimistDigital\MenuBuilder\Classes\MenuLinkable;

use App\Models\Post;

class PostMenuLinkable extends MenuLinkable
{
    public static function getIdentifier(): string
    {
        return 'post';
    }
    
    public static function getName(): string
    {
        return 'Post Link';
    }

    public static function getOptions($locale): array
    {
        return Post::live()->pluck('title', 'id')->toArray();
    }
    
    public static function getDisplayValue($value = null, array $parameters = null)
    {
        $post = Post::find($value);
        return $post->title;
    }

    public static function getValue($value = null, array $parameters = null)
    {
        $post = Post::find($value);
        return route('posts.single', ['slug' => $post->slug]);
    }
}
