<?php

namespace App\Helpers;

use Storage;

class FileHelper
{
    public static function filesizeHumanDivisorUnit($size)
    {
        if ($size >= 1073741824) {
            return ['Gb',1073741824];
        }
    
        if ($size >= 1048576) {
            return ['Mb',1048576];
        } else {
            return ['Kb',1024];
        }
    }
    
    public static function filesizeHuman($filename, $disk = 'public')
    {
        if (!Storage::disk($disk)->exists($filename)) {
            return 0;
        }
    
        $size = Storage::disk($disk)
            ->size($filename);
    
        [$unit, $divisor] = self::filesizeHumanDivisorUnit($size);
    
        return round($size / $divisor, 2) . ' ' . $unit;
    }
}
