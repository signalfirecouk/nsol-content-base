<?php

namespace App\Helpers;

class ProductHelper
{
    public static function price($option)
    {
        return $option->sale_price ?: $option->price;
    }

    public static function picture($product)
    {
        $option = $product
            ->options()
            ->live()
            ->hasStock()
            ->get()
            ->sortBy('purchase_price')
            ->first();
        if ($option && $option->pictures()->exists() && $option->pictures()->count() > 0) {
            return $option
                ->pictures()
                ->first()
                ->filename;
        }
        if ($product->pictures()->exists() && $product->pictures()->count() > 0) {
            return $product
                ->pictures()
                ->first()
                ->filename;
        }
        return 'missing.jpg';
    }

    public static function pictures($product)
    {
        $option = $product->options()
            ->live()
            ->hasStock()
            ->get()
            ->sortBy('purchase_price')
            ->first();

        if ($option->pictures()->count() > 0) {
            return $option->pictures()->get();
        }

        if ($product->pictures()->count() > 0) {
            return $product->pictures()->get();
        }

        //TODO: Replace with default jpg
        return collect();
    }

    public static function isOnSale($option)
    {
        return $option->sale_price ? true : false;
    }

    public static function firstOption($product)
    {
        return $product
            ->options()
            ->live()
            ->hasStock()
            ->orderBy('price', 'ASC')
            ->first();
    }

    public static function optionCount($product)
    {
        return $product
            ->options()
            ->count();
    }
}
