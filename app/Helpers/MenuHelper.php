<?php

namespace App\Helpers;

class MenuHelper
{
    public static function menuItemUrl($item)
    {
        switch (strtolower($item['type'])) {
            case 'static-url':
            case 'post':
            case 'page':
                return $item['value'];
            default: // text
                return '#';
        }
    }

    public static function menuItemLink($item, $url, $classes = [])
    {
        $output = ['<a href="'. $url .'" target="'. $item['target'].'"'];

        if (count($classes) > 0) {
            $output[] = ' class="'. implode(' ', $classes) .'"';
        }

        $output[] = '>';
        $output[] = $item['name'];
        $output[] = '</a>';

        return implode('', $output);
    }

    public static function menuItems($items, $classes = [])
    {
        if (count($classes) > 0) {
            $output = ['<ul class="'. implode(' ', $classes) .'">'];
        } else {
            $output = ['<ul>'];
        }

        foreach ($items as $item) {
            if (!$item['enabled']) {
                continue;
            }
            $url = self::menuItemUrl($item);
            $output[] = '<li>' . self::menuItemLink($item, $url) . '</li>';
        }

        $output[] = '</ul>';

        return $output;
    }

    public static function menuItemsNavBar($items, $child = false)
    {
        $output = [];

        foreach ($items as $item) {
            if (!$item['enabled']) {
                continue;
            }

            $url = self::menuItemUrl($item);
            
            $children = count($item['children']);

            if ($child) {
                $output[] = self::menuItemLink($item, $url, ['dropdown-item']);
            } else {
                if ($children) {
                    $output[] = '<li class="nav-item dropdown">';
                    $output[] = '<a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $item['name'] . '</a>';
                } else {
                    $output[] = '<li class="nav-item">';
                    $output[] = self::menuItemLink($item, $url, ['nav-link']);
                }
            }

            if ($children) {
                $output[] = '<div class="dropdown-menu">';
                $output = array_merge($output, self::menuItemsNavbar($item['children'], true));
                $output[] = '</div>';
            }

            if (!$child) {
                $output[] = '</li>';
            }
        }

        return $output;
    }

    public static function menu($slug, $type = 'default', $classes = [])
    {
        $menu = nova_get_menu($slug);
        if (!$menu) {
            return;
        }
        switch (strtolower($type)) {
            case 'navbar':
                $items = self::menuItemsNavbar($menu['menuItems']);
                break;
            default:
                $items = self::menuItems($menu['menuItems'], $classes);
                break;
        }
        return implode(PHP_EOL, $items);
    }
}
