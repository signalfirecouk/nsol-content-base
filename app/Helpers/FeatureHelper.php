<?php

namespace App\Helpers;

class FeatureHelper
{
    public static function enabled($name, $config)
    {
        return array_keys($config) !== range(0, count($config) - 1) ?
            array_key_exists($name, $config) :
            in_array($name, $config);
    }
}
