<?php

namespace App\Helpers;

use App\Models\Block;

class ContentHelper
{
    public static function nl2p($string)
    {
        $paragraphs = array_map(function ($p) {
            return '<p>' . $p . '</p>';
        }, explode(PHP_EOL, $string));
        return implode(PHP_EOL, $paragraphs);
    }

    public static function blockBySlug($slug)
    {
        $block = Block::live()
            ->where('slug', $slug)
            ->first();
        if (!$block) {
            return 'Block Not Found';
        }
        return $block->content;
    }

    public static function getFontAwesomeClass($filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        switch (strtolower($extension)) {
            case 'pdf':
                return 'fas fa-file-pdf';
            default:
                return 'fas fa-file';
        }
    }
}
