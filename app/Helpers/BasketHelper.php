<?php

namespace App\Helpers;

use Auth;
use NumberFormatter;

use App\Models\BasketItem;
use App\Models\Option;
use App\Models\DeliveryRate;
use App\Models\DeliveryCountry;

use App\Models\Enums\DeliveryType;

class BasketHelper
{
    public static function add($user_id, $identifier, $option, $quantity)
    {
        // Check if the option stock is greater than the quantity wanted
        if (!$option->stock >= $quantity) {
            return false;
        }

        // Create initial data
        $data = [
            'identifier' => $identifier,
            'product_id' => $option->product->id,
            'option_id' => $option->id,
        ];

        // Get or create the item based on initial data
        $item = BasketItem::firstOrCreate(
            $data,
            array_merge($data, [
                'user_id' => $user_id,
                'quantity' => $quantity
            ])
        );

        // Check if existing item. If so then check if the option stock level is
        // greater than that currently in the basket plus that which we want to add
        if (!$item->wasRecentlyCreated) {
            if ($option->stock >= $item->quantity + $quantity) {
                $item->quantity += $quantity;
                return $item->save();
            } else {
                return false;
            }
        }

        return true;
    }

    public static function remove($item)
    {
        return $item->delete();
    }

    public static function increase($item)
    {
        $option = Option::live()->findOrFail($item->option_id);

        if ($item->quantity + 1 > $option->stock) {
            return false;
        }

        $item->quantity = $item->quantity + 1;
        
        return $item->save();
    }

    public static function decrease($item)
    {
        if ($item->quantity - 1 > 0) {
            $item->quantity = $item->quantity - 1;
            return $item->save();
        }
        return $item->delete();
    }

    public static function clear($identifier)
    {
        return BasketItem::where('identifier', $identifier)->delete();
    }

    public static function itemCount($identifier)
    {
        return BasketItem::where('identifier', $identifier)
            ->count();
    }

    public static function valueAsCurrency($value, $locale, $currency)
    {
        $format = new NumberFormatter($locale, NumberFormatter::CURRENCY);
        return $format->formatCurrency($value, $currency);
    }

    public static function valueAsPence($value)
    {
        return $value * 100;
    }

    /**
     * @ToDo - Accurately reflect rules for calculating tax
     */
    public static function tax($item)
    {
        $price = self::price($item);
        $taxes = 0;

        foreach ($item->option->taxes()->get() as $tax) {
            if ($tax->rate === 0) {
                continue;
            }
            $amount = $price / 100 * $tax->rate;
            $taxes += $amount;
        }

        return $taxes;
    }

    public static function taxes($items)
    {
        $taxes = 0;

        foreach ($items as $item) {
            $taxes += self::tax($item);
        }

        return $taxes;
    }

    public static function zoneFromAddress($address)
    {
        $country = DeliveryCountry::where('name', $address->country)->first();

        if (!$country) {
            return false;
        }

        return $country->zone;
    }

    public static function deliveryForItemZone($item, $zone)
    {
        $rates = DeliveryRate::where([
            ['zone', '=', $zone],
            ['type', '!=', DeliveryType::SPENT_ZONE]
        ])->get();
        
        $fees = [];

        foreach ($rates as $rate) {
            if ($rate->type === DeliveryType::WEIGHT_ZONE &&
                $item->option->weight >= $rate->from &&
                $item->option->weight <= $rate->to
            ) {
                $fees[] = $rate->fee * $item->quantity;
            }

            if ($rate->type === DeliveryType::VOLUME_ZONE &&
                ($item->option->width * $item->option->height * $item->option->depth) >= $rate->from &&
                ($item->option->width * $item->option->height * $item->option->depth) <= $rate->to
            ) {
                $fees[] = $rate->fee * $item->quantity;
            }

            if ($rate->type === DeliveryType::FLAT_ZONE) {
                $fees[] = $rate->fee * $item->quantity;
            }
        }

        return count($fees) > 0 ? min($fees) : 0;
    }

    public static function deliveryForBasketSpentZone($items, $zone)
    {
        $total = self::total($items);

        $rates = DeliveryRate::where([
            ['zone', '=', $zone],
            ['type', '=', DeliveryType::SPENT_ZONE]
        ])->get();

        foreach ($rates as $rate) {
            if ($total >= $rate->from && $total <= $rate->to) {
                return $rate->fee;
            }
        }
    }

    public static function delivery($items, $address)
    {
        $zone = self::zoneFromAddress($address);
        
        if (!$zone) {
            return false;
        }

        $fee = self::deliveryForBasketSpentZone($items, $zone);

        if ($fee) {
            return $fee;
        }

        $fees = [];

        foreach ($items as $item) {
            $fees[] = self::deliveryForItemZone($item, $zone);
        }

        return array_sum(array_filter($fees));
    }

    public static function price($item)
    {
        return $item->option->sale_price ?: $item->option->price;
    }

    public static function subtotal($item)
    {
        return self::price($item) * $item->quantity;
    }

    public static function total($items, $extra = 0)
    {
        $total = 0;

        foreach ($items as $item) {
            $total += self::subtotal($item);
        }

        $total += $extra;

        return $total;
    }
}
