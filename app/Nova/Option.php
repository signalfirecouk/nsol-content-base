<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Number;

use Laravel\Nova\Http\Requests\NovaRequest;

use App\Models\Enums\OptionStatus;

class Option extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Option::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = ['id', 'title'];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->onlyOnDetail(),
            BelongsTo::make('Product')
                ->rules('required'),
            Text::make('Title')
                ->rules('required', 'max:1000'),
            Currency::make('Price')
                ->rules('required'),
            Number::make('Stock')
                ->rules('required'),
            Number::make('Weight')
                ->rules('required'),
            Number::make('Width')
                ->rules('required'),
            Number::make('Height')
                ->rules('required'),
            Number::make('Depth')
                ->rules('required'),
            DateTime::make(__('Publish At'), 'publish_at')
                ->rules('nullable')
                ->help(
                    "<div class='flex flex-col'><div class='text-sm font-bold text-primary cursor-pointer dim' style='font-style: normal;' onclick = \"this.parentElement.parentElement.parentElement.querySelector('input')._flatpickr.clear()\" >Clear</div></div>"
                ),
            DateTime::make(__('Unpublish At'), 'unpublish_at')
                ->rules('nullable')
                ->help(
                    "<div class='flex flex-col'><div class='text-sm font-bold text-primary cursor-pointer dim' style='font-style: normal;' onclick = \"this.parentElement.parentElement.parentElement.querySelector('input')._flatpickr.clear()\" >Clear</div></div>"
                ),
            Select::make(__('Status'))
                ->options([
                    OptionStatus::LIVE => __('Live'),
                    OptionStatus::DRAFT => __('Draft'),
                ])
                ->rules('required')
                ->displayUsingLabels(),
            BelongsToMany::make('Pictures'),
            BelongsToMany::make('Taxes'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
