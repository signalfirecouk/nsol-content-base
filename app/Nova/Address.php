<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Address extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Address::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        $address = [
            $this->address1,
            $this->towncity,
            $this->postcode
        ];
        return implode(',', $address);
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'forename', 'surname', 'address1', 'address2', 'address3',
        'address4', 'towncity', 'county', 'postcode', 'telephone',
        'mobile'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->onlyOnDetail(),
            BelongsTo::make(__('User'), 'user')
                ->rules('required'),
            Text::make(__('Forename'), 'forename')
                ->rules('required', 'max:30'),
            Text::make(__('Surname'), 'surname')
                ->rules('required', 'max:30'),
            Text::make(__('Address 1'), 'address1')
                ->rules('required', 'max:50'),
            Text::make(__('Address 2'), 'address2')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Address 3'), 'address3')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Address 4'), 'address4')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Town / City'), 'towncity')
                ->rules('required', 'max:50'),
            Text::make(__('County'), 'county')
                ->rules('required', 'max:50')
                ->hideFromIndex(),
            Text::make(__('Postcode'), 'postcode')
                ->rules('required', 'max:10'),
            Text::make(__('Country'), 'country')
                ->rules('required', 'max:50')
                ->hideFromIndex(),
            Text::make(__('Telephone'), 'telephone')
                ->nullable()
                ->rules('max:30')
                ->hideFromIndex(),
            Text::make(__('Mobile'), 'mobile')
                ->nullable()
                ->rules('max:30')
                ->hideFromIndex(),
            ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
