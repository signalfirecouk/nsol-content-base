<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Panel;

use Laravel\Nova\Http\Requests\NovaRequest;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;

use App\Models\Enums\CategoryStatus;

class Category extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Category::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    protected function categoryFields()
    {
        return [
            TextWithSlug::make(__('Name'))
                ->slug('slug')
                ->rules('required', 'max:100'),
            Slug::make(__('Slug'))
                ->creationRules('required', 'max:100', 'unique:categories,slug')
                ->updateRules('required', 'max:100', 'unique:categories,slug,{{resourceId}}'),
            DateTime::make(__('Publish At'), 'publish_at')
                ->rules('nullable')
                ->help("<div class='flex flex-col'><div class='text-sm font-bold text-primary cursor-pointer dim' style='font-style: normal;' onclick = \"this.parentElement.parentElement.parentElement.querySelector('input')._flatpickr.clear()\" >Clear</div></div>"),
            DateTime::make(__('Unpublish At'), 'unpublish_at')
                ->rules('nullable')->help("<div class='flex flex-col'><div class='text-sm font-bold text-primary cursor-pointer dim' style='font-style: normal;' onclick = \"this.parentElement.parentElement.parentElement.querySelector('input')._flatpickr.clear()\" >Clear</div></div>"),
            Select::make(__('Status'))
                ->options([
                    CategoryStatus::LIVE => __('Live'),
                    CategoryStatus::DRAFT => __('Draft')
                ])
                ->rules('required')
                ->displayUsingLabels(),
        ];
    }

    protected function seoFields()
    {
        return [
            Text::make('Title', 'meta_title')
                ->rules('nullable', 'max:60')
                ->hideFromIndex(),
            Text::make('Description', 'meta_description')
                ->rules('nullable', 'max:160')
                ->hideFromIndex(),

        ];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            new Panel('Category Content', $this->categoryFields()),
            new Panel('SEO Meta', $this->seoFields()),
            BelongsToMany::make('Products'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
