<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

use App\Models\Enums\DeliveryZone;

class DeliveryCountry extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\DeliveryCountry::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name . ', ' . $this->zone;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name','zone'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            Text::make(__('Name'), 'name')->rules('required', 'max:100'),
            Select::make(__('Zone'))
                ->options([
                    DeliveryZone::UK => __('United Kingdom'),
                    DeliveryZone::EUROPE => __('Europe'),
                    DeliveryZone::WORLD_1 => __('World Zone 1'),
                    DeliveryZone::WORLD_2 => __('World Zone 2'),
                    DeliveryZone::WORLD_3 => __('World Zone 3'),
                ])
                ->rules('required')
                ->displayUsingLabels(),
            Number::make(__('Sort'), 'sort')->rules('required')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
