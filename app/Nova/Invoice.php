<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Select;

use Laravel\Nova\Http\Requests\NovaRequest;

use App\Models\Enums\InvoiceStatus;

class Invoice extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Invoice::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->onlyOnDetail(),
            BelongsTo::make(__('User'), 'user'),
            BelongsTo::make(
                __('Payment Address'),
                'paymentAddress',
                'App\Nova\Address'
            ),
            BelongsTo::make(
                __('Delivery Address'),
                'deliveryAddress',
                'App\Nova\Address'
            )->nullable(),
            BelongsTo::make(
                __('Collection Store'),
                'store',
                'App\Nova\Store'
            )->nullable(),
            Text::make(__('Stripe Payment Reference'), 'stripe_payment_id')
                ->hideFromIndex()
                ->nullable(),
            Text::make(__('Identifier'), 'identifier')
                ->hideFromIndex(),
            Currency::make(__('Subtotal'), 'subtotal'),
            Currency::make(__('Delivery'), 'delivery'),
            Currency::make(__('Tax'), 'tax'),
            Currency::make(__('Total'), 'total'),
            Select::make(__('Status'), 'status')
                ->options([
                    InvoiceStatus::PAID => __('Paid'),
                    InvoiceStatus::ON_HOLD  => __('On Hold')])
                ->rules('required')
                ->displayUsingLabels(),
            HasMany::make(__('Invoice Items'), 'invoiceItems', 'App\Nova\InvoiceItem'),
            HasMany::make(__('Invoice Notes'), 'invoiceNotes', 'App\Nova\InvoiceNote'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
