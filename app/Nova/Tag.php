<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;

use App\Models\Enums\TagStatus;

class Tag extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Tag::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Content';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'slug';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'slug'
    ];

    protected function tagFields()
    {
        return [
            TextWithSlug::make(__('Name'))
                ->slug('slug')
                ->rules('required', 'max:100'),
            Slug::make(__('Slug'))
                ->creationRules('required', 'max:100', 'unique:tags,slug')
                ->updateRules('required', 'max:100', 'unique:tags,slug,{{resourceId}}'),
            Select::make(__('Status'))
                ->options([
                    TagStatus::LIVE => 'Live',
                    TagStatus::DRAFT => 'Draft'
                ])
                ->rules('required')
                ->displayUsingLabels(),

        ];
    }

    protected function seoFields()
    {
        return [
            Text::make('Title', 'meta_title')
                ->rules('nullable', 'max:60')
                ->hideFromIndex(),
            Text::make('Description', 'meta_description')
                ->rules('nullable', 'max:160')
                ->hideFromIndex(),

        ];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            new Panel('Tag Content', $this->tagFields()),
            new Panel('SEO Meta', $this->seoFields()),
            BelongsToMany::make('Posts'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
