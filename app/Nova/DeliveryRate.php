<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;

use App\Models\Enums\DeliveryZone;
use App\Models\Enums\DeliveryType;

class DeliveryRate extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\DeliveryRate::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            Select::make(__('Type'))
                ->options([
                    DeliveryType::WEIGHT_ZONE => __('Total weight and zone'),
                    DeliveryType::VOLUME_ZONE => __('Total volume and zone'),
                    DeliveryType::FLAT_ZONE => __('Flat fee by zone'),
                    DeliveryType::SPENT_ZONE => __('Total spent and zone'),
                ])->rules('required')->displayUsingLabels(),
            Number::make(__('From'), 'from')->nullable(),
            Number::make(__('To'), 'to')->nullable(),
            Select::make(__('Zone'), 'zone')
                ->options([
                    DeliveryZone::UK => __('United Kingdom'),
                    DeliveryZone::EUROPE => __('Europe'),
                    DeliveryZone::WORLD_1 => __('World Zone 1'),
                    DeliveryZone::WORLD_2 => __('World Zone 2'),
                    DeliveryZone::WORLD_3 => __('World Zone 3'),
                ])->rules('required')->displayUsingLabels(),

            Currency::make(__('Fee'), 'fee')->rules('required')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
