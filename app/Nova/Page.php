<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Panel;

use Laravel\Nova\Http\Requests\NovaRequest;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;

use App\Models\Enums\PageStatus;

class Page extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Page::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Content';
    
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'slug';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id','title','slug','content'
    ];

    protected function pageFields()
    {
        return [
            TextWithSlug::make(__('Title'))
                ->slug('slug')
                ->rules('required', 'max:1000'),
            Slug::make(__('Slug'))
                ->creationRules('required', 'max:1000', 'unique:pages,slug')
                ->updateRules('required', 'max:1000', 'unique:pages,slug,{{resourceId}}'),
            Trix::make(__('Content'))->rules('required'),
            Select::make(__('Status'))
                ->options([
                    PageStatus::LIVE => __('Live'),
                    PageStatus::DRAFT => __('Draft')
                ])
                ->rules('required')
                ->displayUsingLabels(),
        ];
    }

    protected function seoFields()
    {
        return [
            Text::make('Title', 'meta_title')
                ->rules('nullable', 'max:60')
                ->hideFromIndex(),
            Text::make('Description', 'meta_description')
                ->rules('nullable', 'max:160')
                ->hideFromIndex(),

        ];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            new Panel('Page Content', $this->pageFields()),
            new Panel('SEO Meta', $this->seoFields()),
            BelongsToMany::make('Pictures'),
            BelongsToMany::make('Files')
    
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
