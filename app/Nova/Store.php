<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\KeyValue;
use Laravel\Nova\Http\Requests\NovaRequest;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;

use Signalfirecouk\NovaPointMapField\NovaPointMapField;

class Store extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Store::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'slug', 'identifier', 'address1',
        'towncity', 'county', 'postcode', 'telephone'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            TextWithSlug::make(__('Name'))
                ->slug('slug')
                ->rules('required', 'max:100'),
            Slug::make(__('Slug'))
                ->creationRules('required', 'max:100', 'unique:stores,slug')
                ->updateRules('required', 'max:100', 'unique:stores,slug,{{resourceId}}'),
            Text::make(__('Identifier'), 'identifier')->rules('required'),
            Text::make(__('Address 1'), 'address1')
                ->rules('required', 'max:50'),
            Text::make(__('Address 2'), 'address2')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Address 3'), 'address3')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Address 4'), 'address4')
                ->nullable()
                ->rules('max:50')
                ->hideFromIndex(),
            Text::make(__('Town / City'), 'towncity')
                ->rules('required', 'max:50'),
            Text::make(__('County'), 'county')
                ->rules('required', 'max:50')
                ->hideFromIndex(),
            Text::make(__('Postcode'), 'postcode')
                ->rules('required', 'max:10'),
            NovaPointMapField::make(__('Location'), 'location')
                ->zoom(14)
                ->rules('required'),
            Text::make(__('Country'), 'country')
                ->rules('required', 'max:50')
                ->hideFromIndex(),
            Text::make(__('Telephone'), 'telephone')
                ->nullable()
                ->rules('max:30')
                ->hideFromIndex(),
            KeyValue::make(__('Hours'), 'hours')
                ->rules('required')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
