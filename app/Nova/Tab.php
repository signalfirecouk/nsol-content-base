<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;

use Laravel\Nova\Http\Requests\NovaRequest;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;

use App\Models\Enums\TabStatus;

class Tab extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Tab::class;

    /**
     * The group the resource belongs to
     *
     * @var string
     */
    public static $group = 'Ecommerce';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title', 'slug'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->onlyOnDetail(),
            TextWithSlug::make(__('Title'))
                ->slug('slug')
                ->rules('required', 'max:100'),
            Slug::make(__('Slug'))
                ->creationRules('required', 'max:100', 'unique:tabs,slug')
                ->updateRules('required', 'max:100', 'unique:tabs,slug,{{resourceId}}'),
            Trix::make(__('Content'))->rules('required'),
            Select::make(__('Status'))
                ->options([
                    TabStatus::LIVE => __('Live'),
                    TabStatus::DRAFT => __('Draft')
                ])
                ->rules('required')
                ->displayUsingLabels(),


        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
