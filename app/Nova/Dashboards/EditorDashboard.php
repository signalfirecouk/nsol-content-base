<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Dashboard;

use App\Nova\Metrics\TotalPosts;
use App\Nova\Metrics\TotalPages;

class EditorDashboard extends Dashboard
{
    /**
     * Label for dashboard in menu
     */
    public static function label()
    {
        return 'Editor';
    }

    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            new TotalPosts,
            new TotalPages,
        ];
    }

    /**
     * Get the URI key for the dashboard.
     *
     * @return string
     */
    public static function uriKey()
    {
        return 'editor-dashboard';
    }
}
