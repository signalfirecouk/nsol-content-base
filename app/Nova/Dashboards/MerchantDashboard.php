<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Dashboard;

use App\Nova\Metrics\TotalCategories;
use App\Nova\Metrics\TotalProducts;

class MerchantDashboard extends Dashboard
{
    /**
     * Label for dashboard in menu
     */
    public static function label()
    {
        return 'Merchant';
    }

    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            new TotalProducts,
            new TotalCategories,
        ];
    }

    /**
     * Get the URI key for the dashboard.
     *
     * @return string
     */
    public static function uriKey()
    {
        return 'merchant-dashboard';
    }
}
