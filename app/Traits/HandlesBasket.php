<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Str;

use App\Models\Option;
use App\Models\BasketItem;
use App\Helpers\BasketHelper;
use App\Http\Requests\Ecommerce\AddBasketRequest;

trait HandlesBasket
{

    private function add(Request $request, $id, $quantity)
    {
        $option = Option::live()
            ->findOrFail($id);

        $identifier = $request->cookie('identifier') ?
            $request->cookie('identifier') :
            Str::uuid();

        $user_id = Auth::check() ?
            Auth::user()->id : null;

        if (BasketHelper::add($user_id, $identifier, $option, $quantity)) {
            $level = 'status';
            $message = __(
                ':title added to basket',
                [
                    'title' => $option->product->title
                ]
            );
        } else {
            $level = 'error';
            $message = __(
                'Unable to add :title to basket',
                [
                    'title' => $option->product->title
                ]
            );
        }

        return redirect()
            ->back()
            ->withCookie(cookie('identifier', $identifier, 525600))
            ->with($level, $message);
    }

    public function addViaGet(Request $request, $id, $quantity)
    {
        return $this->add($request, $id, $quantity);
    }

    public function addViaPost(AddBasketRequest $request)
    {
        $validated = $request->validated();
        return $this->add($request, $validated['option'], $validated['quantity']);
    }

    public function increase(Request $request, $id)
    {
        $identifier = $request->cookie('identifier');
        
        if (!$identifier) {
            return redirect()
                ->back()
                ->with(
                    'error',
                    __(
                        'Please enable cookies to proceed'
                    )
                );
        }

        $item = BasketItem::findOrFail($id);

        if (BasketHelper::increase($item)) {
            $level = 'status';
            $message = __(
                ':title quantity increased in basket',
                [
                    'title' => $item->product->title
                ]
            );
        } else {
            $level = 'error';
            $message = __(
                'Unable to increase quantity in your basket'
            );
        }

        return redirect()
            ->back()
            ->with($level, $message);
    }

    public function decrease(Request $request, $id)
    {
        $identifier = $request->cookie('identifier');
        
        if (!$identifier) {
            return redirect()
                ->back()
                ->with(
                    'error',
                    __('Please enable cookies to proceed')
                );
        }
        
        $item = BasketItem::findOrFail($id);

        if (BasketHelper::decrease($item)) {
            $level = 'status';
            $message = __(
                ':title quantity decreased in basket',
                [
                    'title' => $item->product->title
                ]
            );
        } else {
            $level = 'error';
            $message = __(
                'Unable to decrease quantity in your basket'
            );
        }

        return redirect()
            ->back()
            ->with($level, $message);
    }

    public function remove(Request $request, $id)
    {
        $identifier = $request->cookie('identifier');
        
        if (!$identifier) {
            return redirect()
                ->back()
                ->with(
                    'error',
                    __('Please enable cookies to proceed')
                );
        }

        $item = BasketItem::findOrFail($id);

        if (BasketHelper::remove($item)) {
            $level = 'status';
            $message = __(
                ':title removed from basket',
                [
                    'title' => $item->product->title
                ]
            );
        } else {
            $level = 'error';
            $message = __(
                'Unable to remove item from your basket'
            );
        }

        return redirect()
            ->back()
            ->with($level, $message);
    }

    public function clear(Request $request)
    {
        $identifier = $request->cookie('identifier');
        
        if (!$identifier) {
            return redirect()
                ->back()
                ->with(
                    'error',
                    __('Please enable cookies to proceed')
                );
        }

        if (BasketHelper::clear($identifier)) {
            $level = 'status';
            $message = __('Basket items were cleared');
        } else {
            $level = 'error';
            $message = __('Unable to clear basket items');
        }

        return redirect()
            ->back()
            ->with($level, $message);
    }
}
