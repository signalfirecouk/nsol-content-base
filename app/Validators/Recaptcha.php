<?php

namespace App\Validators;

use Log;

use GuzzleHttp\Client;

class ReCaptcha
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $result = false;
        
        try {
            $client = new Client;
            
            $response = $client->post(
                'https://www.google.com/recaptcha/api/siteverify',
                [
                    'form_params' =>
                        [
                            'secret' => config('services.recaptcha.secret'),
                            'response' => $value
                        ]
                ]
            );
            
            $body = json_decode((string)$response->getBody());
            $result = $body->success;
        } catch (Exception $ex) {
            Log::error($ex);
        }

        return $result;
    }
}
