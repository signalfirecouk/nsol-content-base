<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

use Laravel\Nova\Fields\Text;

use Vyuldashev\NovaPermission\NovaPermissionTool;
use OptimistDigital\MenuBuilder\MenuBuilder;

use App\Policies\RolePolicy;
use App\Policies\PermissionPolicy;

use App\Nova\Metrics\TotalUsers;

use App\Nova\Dashboards\MerchantDashboard;
use App\Nova\Dashboards\EditorDashboard;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return $user->hasAnyRole(['Administrator', 'Editor', 'Merchant']);
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            (new TotalUsers)->canSee(function ($request) {
                return $request->user()->hasRole('Administrator');
            }),
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [
            (new MerchantDashboard)->canSee(function ($request) {
                return $request->user()->hasAnyRole('Administrator', 'Merchant');
            }),
            (new EditorDashboard)->canSee(function ($request) {
                return $request->user()->hasAnyRole('Administrator', 'Editor');
            })
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            NovaPermissionTool::make()
                ->rolePolicy(RolePolicy::class)
                ->permissionPolicy(PermissionPolicy::class),
            new MenuBuilder,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
