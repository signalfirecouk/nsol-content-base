<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Http\Request;

use App\Models\BasketItem;

class UserLoggedInListener
{
    public $request;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Login  $event
     * @return void
     */
    public function handle($event)
    {
        $identifier = $this->request->cookie('identifier');

        if (!$identifier) {
            return;
        }

        BasketItem::where('identifier', $identifier)
            ->whereNull('user_id')
            ->update([
                'user_id' => $this->request->user()->id
            ]);
    }
}
