<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Http\Request;

use App\Models\BasketItem;

class UserLoggedOutListener
{
    public $request;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $identifier = $this->request->cookie('identifier');

        if (!$identifier) {
            return;
        }

        BasketItem::where('identifier', $identifier)
            ->whereNotNull('user_id')
            ->update([
                'user_id' => null
            ]);
    }
}
