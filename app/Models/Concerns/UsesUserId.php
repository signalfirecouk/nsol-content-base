<?php

namespace App\Models\Concerns;

trait UsesUserId
{
    protected static function bootUsesUserId()
    {
        static::creating(function ($model) {
            if ($model->user_id) {
                return;
            }
            $model->user_id = auth()->user()->id;
        });
    }
}
