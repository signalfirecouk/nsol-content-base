<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\User;
use App\Models\Tag;
use App\Models\Picture;
use App\Models\PicturePost;
use App\Models\FilePost;

use App\Models\Enums\PostStatus;

class Post extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'title', 'slug','excerpt', 'content', 'publish_at', 'unpublish_at'
    ];

    protected $casts = [
        'publish_at' => 'datetime',
        'unpublish_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function pictures()
    {
        return $this->belongsToMany(Picture::class)
            ->using(PicturePost::class);
    }

    public function files()
    {
        return $this->belongsToMany(File::class)
            ->using(FilePost::class);
    }

    public function scopeLive($query)
    {
        $query = $query->where('status', PostStatus::LIVE);
        $query = $query->where(function ($query) {
            return $query->whereNull('publish_at')
                ->orWhere('publish_at', '<=', Carbon::now());
        });
        $query = $query->where(function ($query) {
            return $query->whereNull('unpublish_at')
                ->orWhere('unpublish_at', '>=', Carbon::now());
        });
        return $query;
    }
}
