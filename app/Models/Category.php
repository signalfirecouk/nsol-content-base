<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\Product;

use App\Models\Enums\CategoryStatus;

class Category extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'parent_category_id', 'name', 'slug', 'meta_title', 'meta_description',
        'publish_at', 'unpublish_at'
    ];

    protected $casts = [
        'publish_at' => 'datetime',
        'unpublish_at' => 'datetime'
    ];
    
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
    
    public function scopeLive($query)
    {
        $query = $query->where('status', CategoryStatus::LIVE);
        $query = $query->where(function ($query) {
            return $query->whereNull('publish_at')
                ->orWhere('publish_at', '<=', Carbon::now());
        });
        $query = $query->where(function ($query) {
            return $query->whereNull('unpublish_at')
                ->orWhere('unpublish_at', '>=', Carbon::now());
        });
        return $query;
    }
}
