<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Spatie\Permission\Traits\HasRoles;

use App\Models\Concerns\UsesUuid;

use App\Models\Post;
use App\Models\Page;
use App\Models\Block;
use App\Models\Picture;
use App\Models\File;
use App\Models\Basket;
use App\Models\Invoice;
use App\Models\Address;

class User extends Authenticatable
{
    use Notifiable, UsesUuid, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'forename', 'surname', 'email', 'nickname', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    public function blocks()
    {
        return $this->hasMany(Block::class);
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function baskets()
    {
        return $this->hasMany(Basket::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
