<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

use App\Models\Concerns\UsesUuid;
use App\Models\Enums\StoreStatus;
use App\Models\Invoice;

class Store extends Model
{
    use UsesUuid, SpatialTrait;

    protected $fillable = [
        'name', 'slug', 'identifier', 'address1', 'address2', 'address3',
        'address4', 'towncity', 'county', 'postcode', 'country', 'location',
        'telephone', 'email', 'hours'
    ];

    protected $spatialFields = [
        'location',
    ];

    protected $casts = [
        'hours' => 'array'
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function scopeLive($query)
    {
        return $query->where('status', StoreStatus::LIVE);
    }
}
