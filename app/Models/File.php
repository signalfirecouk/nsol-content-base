<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\Post;
use App\Models\FilePost;
use App\Models\User;

class File extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'filename', 'description'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class)
            ->using(FilePost::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
