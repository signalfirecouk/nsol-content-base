<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\User;

class Address extends Model
{
    use UsesUuid;

    protected $fillable = [
        'user_id', 'forename', 'surname', 'address1', 'address2',
        'address3', 'address4', 'towncity', 'county', 'postcode',
        'country', 'telephone', 'mobile'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
