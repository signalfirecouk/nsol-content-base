<?php

namespace App\Models\Enums;

class InvoiceNoteVisibility
{
    const CUSTOMER = 1;
    const STAFF = 0;
}
