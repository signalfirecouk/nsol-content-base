<?php

namespace App\Models\Enums;

class PostStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
