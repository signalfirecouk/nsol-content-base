<?php

namespace App\Models\Enums;

class TagStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
