<?php

namespace App\Models\Enums;

class BlockStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
