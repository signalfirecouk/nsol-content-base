<?php

namespace App\Models\Enums;

class InvoiceStatus
{
    const PAID = 1;
    const ON_HOLD = 0;
}
