<?php

namespace App\Models\Enums;

class OptionStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
