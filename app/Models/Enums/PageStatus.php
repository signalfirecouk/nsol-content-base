<?php

namespace App\Models\Enums;

class PageStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
