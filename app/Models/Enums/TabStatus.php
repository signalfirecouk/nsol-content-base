<?php

namespace App\Models\Enums;

class TabStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
