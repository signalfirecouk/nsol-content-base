<?php

namespace App\Models\Enums;

class DeliveryType
{
    const WEIGHT_ZONE = 'weight-zone';
    const VOLUME_ZONE = 'volume-zone';
    const FLAT_ZONE = 'flat-zone';
    const SPENT_ZONE = 'spent-zone';
}
