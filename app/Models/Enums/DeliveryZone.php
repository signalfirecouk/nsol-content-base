<?php

namespace App\Models\Enums;

class DeliveryZone
{
    const UK = 'United Kingdom';
    const EUROPE = 'Europe';
    const WORLD_1 = 'World Zone 1';
    const WORLD_2 = 'World Zone 2';
    const WORLD_3 = 'World Zone 3';
}
