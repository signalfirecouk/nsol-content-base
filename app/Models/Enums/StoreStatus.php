<?php

namespace App\Models\Enums;

class StoreStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
