<?php

namespace App\Models\Enums;

class CategoryStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
