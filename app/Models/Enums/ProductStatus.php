<?php

namespace App\Models\Enums;

class ProductStatus
{
    const LIVE = 1;
    const DRAFT = 0;
}
