<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\Post;

use App\Models\Enums\TagStatus;

class Tag extends Model
{
    use UsesUuid;

    protected $fillable = [
        'name', 'slug', 'status'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function scopeLive($query)
    {
        return $query->where('status', TagStatus::LIVE);
    }
}
