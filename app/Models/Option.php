<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\Product;
use App\Models\OptionPicture;
use App\Models\Basket;
use App\Models\Tax;

use App\Models\Enums\OptionStatus;

use ProductHelper;

class Option extends Model
{
    use UsesUuid, UsesUserId;

    protected $casts = [
        'publish_at' => 'datetime',
        'unpublish_at' => 'datetime'
    ];

    protected $appends = [
        'purchase_price'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function pictures()
    {
        return $this->belongsToMany(Picture::class)
            ->using(OptionPicture::class);
    }

    public function baskets()
    {
        return $this->hasMany(Basket::class);
    }

    public function taxes()
    {
        return $this->belongsToMany(Tax::class);
    }

    public function scopeHasStock($query)
    {
        return $query->where('stock', '>', 0);
    }

    public function getPurchasePriceAttribute()
    {
        return ProductHelper::price($this);
    }

    public function scopeLive($query)
    {
        $query = $query->where('status', OptionStatus::LIVE);
        $query = $query->where(function ($query) {
            return $query->whereNull('publish_at')
                ->orWhere('publish_at', '<=', Carbon::now());
        });
        $query = $query->where(function ($query) {
            return $query->whereNull('unpublish_at')
                ->orWhere('unpublish_at', '>=', Carbon::now());
        });
        return $query;
    }
}
