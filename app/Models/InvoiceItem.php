<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\Invoice;
use App\Models\Product;
use App\Models\Option;

class InvoiceItem extends Model
{
    use UsesUuid;

    protected $fillable = [
        'product_id', 'option_id', 'quantity', 'price'
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}
