<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Option;
use App\Models\DeliveryRate;

use App\Models\Concerns\UsesUuid;

class Tax extends Model
{
    use UsesUuid;

    protected $fillable = [
        'name', 'amount'
    ];

    public function options()
    {
        return $this->belongsToMany(Option::class);
    }

    public function deliveryRates()
    {
        return $this->hasMany(DeliveryRate::class);
    }
}
