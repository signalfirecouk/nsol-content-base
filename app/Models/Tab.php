<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\User;

use App\Models\Enums\TabStatus;

class Tab extends Model
{
    use UsesUuid, UsesUserId;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeLive($query)
    {
        return $query->where('status', TabStatus::LIVE);
    }
}
