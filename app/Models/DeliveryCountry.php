<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

class DeliveryCountry extends Model
{
    use UsesUuid;

    protected $fillable = [
        'name', 'zone', 'sort'
    ];
}
