<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\Invoice;

use App\Models\Enums\InvoiceNoteVisiblity;

class InvoiceNote extends Model
{
    use UsesUuid;

    protected $fillable = [
        'invoice_id', 'visiblity', 'content'
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
