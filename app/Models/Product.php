<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\Category;
use App\Models\Option;
use App\Models\PictureProduct;
use App\Models\Basket;
use App\Models\Tab;

use App\Models\Enums\ProductStatus;

class Product extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'title', 'slug', 'summary', 'description',
        'meta_title', 'meta_description', 'publish_at', 'unpublish_at'
    ];

    protected $casts = [
        'publish_at' => 'datetime',
        'unpublish_at' => 'datetime'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function pictures()
    {
        return $this->belongsToMany(Picture::class)
            ->using(PictureProduct::class);
    }

    public function baskets()
    {
        return $this->hasMany(Basket::class);
    }

    public function tabs()
    {
        return $this->hasMany(Tab::class);
    }

    public function scopeLive($query)
    {
        $query = $query->where('status', ProductStatus::LIVE);
        $query = $query->where(function ($query) {
            return $query->whereNull('publish_at')
                ->orWhere('publish_at', '<=', Carbon::now());
        });
        $query = $query->where(function ($query) {
            return $query->whereNull('unpublish_at')
                ->orWhere('unpublish_at', '>=', Carbon::now());
        });
        return $query;
    }
}
