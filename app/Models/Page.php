<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\User;
use App\Models\Picture;
use App\Models\File;
use App\Models\PagePicture;
use App\Models\FilePage;

use App\Models\Enums\PageStatus;

class Page extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'title', 'slug', 'content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pictures()
    {
        return $this->belongsToMany(Picture::class)
            ->using(PagePicture::class);
    }

    public function files()
    {
        return $this->belongsToMany(File::class)
            ->using(FilePage::class);
    }

    public function scopeLive($query)
    {
        return $query->where('status', PageStatus::LIVE);
    }
}
