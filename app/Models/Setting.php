<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

class Setting extends Model
{
    use UsesUuid;

    protected $fillable = [
        'key', 'value'
    ];
}
