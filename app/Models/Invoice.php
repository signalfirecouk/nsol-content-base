<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\User;
use App\Models\Address;
use App\Models\InvoiceItem;
use App\Models\InvoiceNote;
use App\Models\Store;

use App\Models\Enums\InvoiceStatus;

class Invoice extends Model
{
    use UsesUuid;
    
    protected $fillable = [
        'user_id', 'identifier', 'payment_address_id', 'delivery_address_id',
        'store_id', 'stripe_payment_id', 'subtotal', 'delivery', 'tax', 'total', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function deliveryAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function paymentAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function invoiceItems()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function invoiceNotes()
    {
        return $this->hasMany(InvoiceNote::class);
    }

    public function scopeOnHold($query)
    {
        return $query->where('status', InvoiceStatus::ON_HOLD);
    }

    public function scopePaid($query)
    {
        return $query->where('status', InvoiceStatus::PAID);
    }
}
