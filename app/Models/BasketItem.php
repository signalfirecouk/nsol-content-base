<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\User;
use App\Models\Product;
use App\Models\Option;

class BasketItem extends Model
{
    use UsesUuid;

    protected $fillable = [
        'identifier', 'user_id', 'product_id', 'option_id', 'quantity'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}
