<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;
use App\Models\Concerns\UsesUserId;

use App\Models\Post;
use App\Models\PicturePost;
use App\Models\PagePicture;
use App\Models\User;

class Picture extends Model
{
    use UsesUuid, UsesUserId;

    protected $fillable = [
        'filename', 'caption'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class)
            ->using(PicturePost::class);
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class)
            ->using(PagePicture::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
