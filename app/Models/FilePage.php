<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class FilePage extends Pivot implements Sortable
{
    use SortableTrait;

    public $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    public $sortable = [
        'order_column_name' => 'sort_order',
        'sort_when_creating' => true,
    ];

    public function buildSortQuery()
    {
        return static::query()
          ->where('file_id', $this->file_id);
    }
}
