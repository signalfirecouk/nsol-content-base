<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Concerns\UsesUuid;

use App\Models\Tax;

class DeliveryRate extends Model
{
    use UsesUuid;

    protected $fillable = [
        'type', 'from', 'to', 'zone', 'fee'
    ];

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }
}
