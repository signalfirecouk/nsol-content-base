<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->string('title',1000);
            $table->string('slug',1000);
            $table->string('summary', 2000)->nullable();
            $table->text('description');
            $table->string('meta_title',60)->nullable();
            $table->string('meta_description', 160)->nullable();
            $table->datetime('publish_at')->nullable();
            $table->datetime('unpublish_at')->nullable();
            $table->unsignedTinyInteger('status');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
