<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('parent_category_id')->nullable();
            $table->string('name', 100)->index();
            $table->string('slug', 100)->unique();
            $table->string('meta_title',60)->nullable();
            $table->string('meta_description', 160)->nullable();
            $table->datetime('publish_at')->nullable();
            $table->datetime('unpublish_at')->nullable();            
            $table->unsignedTinyInteger('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parent_category_id')->references('id')->on('categories');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
