<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_rates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('tax_id');
            $table->string('type', 30);
            $table->decimal('from', 8, 2)->nullable();
            $table->decimal('to', 8, 2)->nullable();
            $table->string('zone', 30);
            $table->decimal('fee', 8, 2);
            $table->timestamps();

            $table->foreign('tax_id')->references('id')->on('taxes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_rates');
    }
}
