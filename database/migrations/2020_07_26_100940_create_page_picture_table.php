<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagePictureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_picture', function (Blueprint $table) {
            $table->uuid('picture_id');
            $table->uuid('page_id');
            $table->integer('sort_order')->default(0);
            $table->foreign('picture_id')->references('id')->on('pictures');
            $table->foreign('page_id')->references('id')->on('pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_picture');
    }
}
