<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');    
            $table->uuid('delivery_address_id')->nullable();
            $table->uuid('store_id')->nullable();
            $table->uuid('payment_address_id');            
            $table->string('stripe_payment_id',50)->nullable();
            $table->uuid('identifier');       
            $table->decimal('subtotal', 10, 2); 
            $table->decimal('delivery', 10, 2);
            $table->decimal('tax', 10, 2);
            $table->decimal('total', 10, 2);
            $table->unsignedTinyInteger('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('delivery_address_id')->references('id')->on('addresses');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('payment_address_id')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
