<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Store;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Point;

$factory->define(Store::class, function (Faker $faker) {
    $name = implode(' ', $faker->words(3));
    $countries = ['England', 'Scotland', 'Wales', 'Northern Ireland'];
    return [
        'name' => ucfirst($name),
        'slug' => Str::slug($name),
        'identifier' => Str::slug($name) . '-' . $faker->randomNumber(2),
        'address1' => $faker->streetAddress,
        'towncity' => $faker->city,
        'county' => $faker->county,
        'postcode' => $faker->postcode,
        'country' => $countries[array_rand($countries)],
        'location' => new Point($faker->longitude, $faker->latitude),
        'telephone' => $faker->phoneNumber,
        'email' => $faker->safeEmail(),
        'hours' => [
            'Monday' => '9am to 6pm',
            'Tuesday' => '9am to 6pm',
            'Wednesday' => '9am to 6pm',
            'Thursday' => '9am to 6pm',
            'Friday' => '9am to 6pm',
            'Saturday' => '9am to 6pm',
            'Sunday' => '10am to 4pm',
        ]
    ];
});

$factory->state(Store::class, 'live', [
    'status' => 1
]);
