<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

use OptimistDigital\MenuBuilder\Models\MenuItem;

$factory->define(MenuItem::class, function (Faker $faker) {
    return [
        'name' => implode(' ', $faker->words(2)),
        'class' => 'OptimistDigital\MenuBuilder\Classes\MenuItemStaticURL',
        'value' => '/',
        'target' => '_self',
    ];
});

$factory->state(MenuItem::class, 'enabled', [
    'enabled' => 1
]);

$factory->state(MenuItem::class, 'disabled', [
    'enabled' => 0
]);
