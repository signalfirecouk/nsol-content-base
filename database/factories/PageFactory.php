<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(8),
        'slug' => $faker->slug(),
        'content' => implode(\PHP_EOL, $faker->paragraphs(10)),
    ];
});

$factory->state(Page::class, 'meta', function ($faker) {
    return [
        'meta_title' => $faker->text(50),
        'meta_description' => $faker->text(100)
    ];
});

$factory->state(Page::class, 'live', [
    'status' => 1
]);

$factory->state(Page::class, 'draft', [
    'status' => 0
]);
