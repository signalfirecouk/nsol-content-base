<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

use OptimistDigital\MenuBuilder\Models\Menu;

$factory->define(Menu::class, function (Faker $faker) {
    return [
        'name' => implode(' ', $faker->words(2)),
        'slug' => $faker->slug,
        'locale' => 'en'
    ];
});
