<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tab;
use Faker\Generator as Faker;

$factory->define(Tab::class, function (Faker $faker) {
    return [
        'title' => implode(' ', $faker->words(2)),
        'slug' => $faker->slug,
        'content' => $faker->sentence(100)
    ];
});

$factory->state(Tab::class, 'live', [
    'status' => 1
]);

$factory->state(Tab::class, 'draft', [
    'status' => 0
]);
