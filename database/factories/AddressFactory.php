<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'forename' => $faker->firstName,
        'surname' => $faker->lastName,
        'address1' => $faker->streetAddress,
        'towncity' => $faker->city,
        'county' => $faker->county,
        'postcode' => $faker->postcode,
        'country' => array_rand(['England', 'Scotland', 'Wales', 'Northern Ireland']),
        'telephone' => $faker->phoneNumber
    ];
});
