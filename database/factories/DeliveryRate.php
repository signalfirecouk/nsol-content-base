<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryRate;
use Faker\Generator as Faker;

$factory->define(DeliveryRate::class, function (Faker $faker) {
    return [
        'type' => array_rand(['weight-zone', 'volume-zone', 'flat-zone', 'spent-zone']),
        'from' => rand(1, 100),
        'to' => rand(1, 200),
        'zone' => array_rand(['United Kingdom', 'Europe', 'World 1', 'World 2', 'World 3']),
        'fee' => $faker->randomFloat(2),
    ];
});
