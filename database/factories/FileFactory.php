<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\File;
use Faker\Generator as Faker;

$factory->define(File::class, function (Faker $faker) {
    return [
        'filename' => $faker->file('storage/app/test', 'public/storage', false),
        'description' => implode(' ', $faker->words(8))
    ];
});
