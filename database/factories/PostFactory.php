<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(8),
        'slug' => $faker->slug(),
        'content' => implode(\PHP_EOL, $faker->paragraphs(5))
    ];
});

$factory->state(Post::class, 'excerpt', function ($faker) {
    return [
        'excerpt' => $faker->paragraph(1)
    ];
});

$factory->state(Post::class, 'meta', function ($faker) {
    return [
        'meta_title' => $faker->text(50),
        'meta_description' => $faker->text(100)
    ];
});

$factory->state(Post::class, 'live', [
    'status' => 1
]);

$factory->state(Post::class, 'draft', [
    'status' => 0
]);
