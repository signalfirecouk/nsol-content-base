<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Option;
use Faker\Generator as Faker;

$factory->define(Option::class, function (Faker $faker) {
    return [
        'title' => ucfirst(implode(' ', $faker->words(2))),
        'price' => $faker->randomFloat(2, 50, 100),
        'stock' => $faker->randomDigit(),
        'weight' => $faker->randomFloat(2, 0.5, 10),
        'width' => $faker->randomFloat(2, 1, 100),
        'height' => $faker->randomFloat(2, 1, 100),
        'depth' => $faker->randomFloat(2, 1, 100),
    ];
});

$factory->state(Option::class, 'live', [
    'status' => 1
]);

$factory->state(Option::class, 'draft', [
    'status' => 0
]);

$factory->state(Option::class, 'outofstock', [
    'stock' => 0
]);

$factory->state(Option::class, 'onsale', function ($faker) {
    return [
        'sale_price' => $faker->randomFloat(2, 5, 49),
    ];
});
