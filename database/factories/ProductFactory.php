<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => ucfirst(implode(' ', $faker->words(2))),
        'slug' => $faker->slug(),
        'summary' => $faker->paragraph(1),
        'description' =>  implode(\PHP_EOL, $faker->paragraphs(5)),
    ];
});

$factory->state(Product::class, 'live', [
    'status' => 1
]);

$factory->state(Product::class, 'draft', [
    'status' => 0
]);

$factory->state(Product::class, 'meta', function ($faker) {
    return [
        'meta_title' => $faker->text(50),
        'meta_description' => $faker->text(100)
    ];
});
