<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryCountry;
use Faker\Generator as Faker;

$factory->define(DeliveryCountry::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'zone' => array_rand(['United Kingdom', 'Europe', 'World 1', 'World 2', 'World 3']),
    ];
});
