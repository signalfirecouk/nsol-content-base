<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Block;
use Faker\Generator as Faker;

$factory->define(Block::class, function (Faker $faker) {
    return [
        'name' => implode(' ', $faker->words(2)),
        'slug' => $faker->slug,
        'content' => $faker->sentence(100)
    ];
});

$factory->state(Block::class, 'live', [
    'status' => 1
]);

$factory->state(Block::class, 'draft', [
    'status' => 0
]);
