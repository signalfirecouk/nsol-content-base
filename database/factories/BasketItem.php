<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BasketItem;
use Faker\Generator as Faker;

$factory->define(BasketItem::class, function (Faker $faker) {
    return [
        'identifier' => Str::uuid(),
        'quantity' => $faker->randomDigit()
    ];
});
