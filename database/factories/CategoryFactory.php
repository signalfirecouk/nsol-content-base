<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => ucfirst(implode(' ', $faker->words(2))),
        'slug' => $faker->slug()
    ];
});

$factory->state(Category::class, 'live', [
    'status' => 1
]);

$factory->state(Category::class, 'draft', [
    'status' => 0
]);

$factory->state(Category::class, 'meta', function ($faker) {
    return [
        'meta_title' => $faker->text(50),
        'meta_description' => $faker->text(100)
    ];
});
