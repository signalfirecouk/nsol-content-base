<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        'name' => ucfirst(implode(' ', $faker->words(2))),
        'slug' => $faker->slug()
    ];
});

$factory->state(Tag::class, 'meta', function ($faker) {
    return [
        'meta_title' => $faker->text(50),
        'meta_description' => $faker->text(100)
    ];
});

$factory->state(Tag::class, 'active', [
    'status' => 1
]);

$factory->state(Tag::class, 'inactive', [
    'status' => 0
]);
