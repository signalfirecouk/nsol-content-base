<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Setting;
use Faker\Generator as Faker;

use Illuminate\Support\Str;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        'key' => Str::slug(implode(' ', $faker->words(2))),
        'value' => implode(' ', $faker->words(10))
    ];
});
