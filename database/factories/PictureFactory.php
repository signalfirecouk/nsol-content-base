<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Picture;
use Faker\Generator as Faker;

$factory->define(Picture::class, function (Faker $faker) {
    return [
        'filename' => $faker->image('public/storage', 640, 480, null, false),
        'caption' => implode(' ', $faker->words(8))
    ];
});
