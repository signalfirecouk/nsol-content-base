<?php

use Illuminate\Database\Seeder;

use App\Models\Product;
use App\Models\Picture;

class PictureProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        foreach($products as $product)
        {
            $pictures = Picture::inRandomOrder()
                ->limit(rand(0,5))
                ->pluck('id')
                ->toArray();
            
            if (count($pictures) > 0) {
                $product->pictures()->sync($pictures);
            }
        }
    }
}
