<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;

use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName('Merchant');
        $user = $role->users()->first();

        factory(Product::class, rand(10, 20))->states('meta','live')->create([
            'user_id' => $user->id
        ]);
    }
}
