<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;

use Illuminate\Support\Arr;

use App\Models\Option;
use App\Models\Product;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName('Merchant');
        $user = $role->users()->first();

        $products = Product::all();

        foreach($products as $product) {
            $states = ['live'];
            if (rand(1, 100) % 2 === 0){
                $states[] = 'onsale';
            }
            factory(Option::class, rand(1,10))->states(...$states)->create([
                'user_id' => $user->id,
                'product_id' => $product->id
            ]);
        }
    }
}
