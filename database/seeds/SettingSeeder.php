<?php

use Illuminate\Database\Seeder;

use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Setting::class)->create([
            'key' => 'facebook_url',
            'value' => 'https://facebook.com/example'
        ]);

        factory(Setting::class)->create([
            'key' => 'twitter_url',
            'value' => 'https://twitter.com/example'
        ]);
    }
}
