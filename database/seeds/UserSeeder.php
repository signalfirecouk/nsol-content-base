<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = Role::findByName('Administrator');
        $merchant = Role::findByName('Merchant');
        $editor = Role::findByName('Editor');
        $public = Role::findByName('Public');

        $administrator->users()->attach(factory(User::class, 1)->create(
            ['email' => 'administrator@example.com']
        ));

        $merchant->users()->attach(factory(User::class, 1)->create(
            ['email' => 'merchant@example.com']
        ));        
        
        $editor->users()->attach(factory(User::class, 1)->create(
            ['email' => 'editor@example.com']
        ));    

        $public->users()->attach(factory(User::class, 1)->create(
            ['email' => 'public@example.com']
        ));        

    }
}
