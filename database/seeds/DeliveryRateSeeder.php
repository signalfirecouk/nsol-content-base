<?php

use Illuminate\Database\Seeder;

use App\Models\DeliveryRate;
use App\Models\Tax;

class DeliveryRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxes = Tax::all();

        $tax_free = Tax::where('rate', 0)->first();

        factory(DeliveryRate::class)->create([
            'type' => 'weight-zone',
            'from' => 0,
            'to' => 10,
            'fee' => 3.95,
            'zone' => 'United Kingdom',
            'tax_id' => $taxes->random()->id
        ]);

        factory(DeliveryRate::class)->create([
            'type' => 'weight-zone',
            'from' => 10,
            'to' => 20,
            'fee' => 4.95,
            'zone' => 'United Kingdom',
            'tax_id' => $taxes->random()->id
        ]);        

        factory(DeliveryRate::class)->create([
            'type' => 'weight-zone',
            'from' => 20,
            'to' => 40,
            'fee' => 5.95,
            'zone' => 'United Kingdom',
            'tax_id' => $taxes->random()->id
        ]);     

        factory(DeliveryRate::class)->create([
            'type' => 'weight-zone',
            'from' => 40,
            'to' => 1000,
            'fee' => 6.95,
            'zone' => 'United Kingdom',
            'tax_id' => $taxes->random()->id
        ]); 
        
        factory(DeliveryRate::class)->create([
            'type' => 'spent-zone',
            'from' => 50,
            'to' => 100000,
            'fee' => 0,
            'zone' => 'United Kingdom',
            'tax_id' => $tax_free->id
        ]);         
    }
}
