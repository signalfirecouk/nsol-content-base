<?php

use Illuminate\Database\Seeder;

use App\Models\Tax;

class TaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tax::class)->create([
            'name' => 'VAT (Standard)',
            'rate' => 20
        ]);

        factory(Tax::class)->create([
            'name' => 'VAT (Reduced)',
            'rate' => 5
        ]);

        factory(Tax::class)->create([
            'name' => 'VAT (Zero)',
            'rate' => 0
        ]);
    }
}
