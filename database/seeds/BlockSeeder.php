<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;

use App\Models\Block;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName('Administrator');
        $user = $role->users()->first();

        factory(Block::class)->states('live')->create([
            'user_id' => $user->id,
            'name' => 'Address',
            'slug' => 'address',
            'content' => '58  Earls Avenue<br/>Whiteshill<br/>GL6 6DB'
        ]);

        factory(Block::class)->states('live')->create([
            'user_id' => $user->id,
            'name' => 'Copyright',
            'slug' => 'copyright',
            'content' => 'Company Name. All Rights Reserved'
        ]);

        factory(Block::class)->states('live')->create([
            'user_id' => $user->id,
            'name' => 'Homepage',
            'slug' => 'homepage',
        ]);


    }
}
