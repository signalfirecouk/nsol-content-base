<?php

use Illuminate\Database\Seeder;

use App\Models\Option;
use App\Models\Tax;

class OptionTaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = Option::all();

        foreach($options as $option)
        {
            $tax = Tax::inRandomOrder()
                ->limit(1)
                ->pluck('id')
                ->toArray();

            $option->taxes()->sync($tax);
        }
    }
}
