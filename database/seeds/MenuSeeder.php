<?php

use Illuminate\Database\Seeder;

use OptimistDigital\MenuBuilder\Models\Menu;
use OptimistDigital\MenuBuilder\Models\MenuItem;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = factory(Menu::class)->create([
            'name' => 'Default',
            'slug' => 'default',
        ]);

        factory(MenuItem::class)->states('enabled')->create([
            'menu_id' => $menu->id,
            'name' => 'Home',
            'value' => '/',
            'order' => 1
        ]);

        factory(MenuItem::class)->states('enabled')->create([
            'menu_id' => $menu->id,
            'name' => 'Blog',
            'value' => '/blog',
            'order' => 2
        ]);

        factory(MenuItem::class)->states('enabled')->create([
            'menu_id' => $menu->id,
            'name' => 'Contact',
            'value' => '/contact',
            'order' => 3
        ]);        

        if (FeatureHelper::enabled('ecommerce', config('site.features'))) {
            factory(MenuItem::class)->states('enabled')->create([
                'menu_id' => $menu->id,
                'name' => 'Shop',
                'value' => '/shop',
                'order' => 4
            ]);        
        }
    }
}
