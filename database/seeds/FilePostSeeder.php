<?php

use Illuminate\Database\Seeder;

use App\Models\Post;
use App\Models\File;

class FilePostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();

        foreach($posts as $post)
        {
            $files = File::inRandomOrder()
                ->limit(rand(0,10))
                ->pluck('id')
                ->toArray();
            
            if (count($files) > 0) {
                $post->files()->sync($files);
            }
        }
    }
}
