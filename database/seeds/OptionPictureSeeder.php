<?php

use Illuminate\Database\Seeder;

use App\Models\Option;
use App\Models\Picture;

class OptionPictureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = Option::all();

        foreach($options as $option)
        {
            $pictures = Picture::inRandomOrder()
                ->limit(rand(0,5))
                ->pluck('id')
                ->toArray();
            
            if (count($pictures) > 0) {
                $option->pictures()->sync($pictures);
            }
        }    }
}
