<?php

use Illuminate\Database\Seeder;

use App\Models\DeliveryCountry;

class DeliveryCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'England', 'zone' => 'United Kingdom'],
            ['name' => 'Scotland', 'zone' => 'United Kingdom'],
            ['name' => 'Wales', 'zone' => 'United Kingdom'],
            ['name' => 'Northern Ireland', 'zone' => 'United Kingdom'],
            ['name' => 'Albania', 'zone' => 'Europe'],
            ['name' => 'Denmark', 'zone' => 'Europe'],
            ['name' => 'Kyrgyzstan', 'zone' => 'Europe'],
            ['name' => 'Russia', 'zone' => 'Europe'],
            ['name' => 'Andorra', 'zone' => 'Europe'],
            ['name' => 'Estonia', 'zone' => 'Europe'],
            ['name' => 'Latvia', 'zone' => 'Europe'],
            ['name' => 'San Marino', 'zone' => 'Europe'],
            ['name' => 'Armenia', 'zone' => 'Europe'],
            ['name' => 'Faroe Islands', 'zone' => 'Europe'],
            ['name' => 'Liechtenstein', 'zone' => 'Europe'],
            ['name' => 'Serbia', 'zone' => 'Europe'],
            ['name' => 'Austria', 'zone' => 'Europe'],
            ['name' => 'Finland', 'zone' => 'Europe'],
            ['name' => 'Lithuania', 'zone' => 'Europe'],
            ['name' => 'Slovakia', 'zone' => 'Europe'],
            ['name' => 'Azerbaijan', 'zone' => 'Europe'],
            ['name' => 'France', 'zone' => 'Europe'],
            ['name' => 'Luxembourg', 'zone' => 'Europe'],
            ['name' => 'Slovenia', 'zone' => 'Europe'],
            ['name' => 'Azores', 'zone' => 'Europe'],
            ['name' => 'Georgia', 'zone' => 'Europe'],
            ['name' => 'Macedonia', 'zone' => 'Europe'],
            ['name' => 'Spain', 'zone' => 'Europe'],
            ['name' => 'Balearic Islands', 'zone' => 'Europe'],
            ['name' => 'Germany', 'zone' => 'Europe'],
            ['name' => 'Madeira', 'zone' => 'Europe'],
            ['name' => 'Sweden', 'zone' => 'Europe'],
            ['name' => 'Belarus', 'zone' => 'Europe'],
            ['name' => 'Gibraltar', 'zone' => 'Europe'],
            ['name' => 'Malta', 'zone' => 'Europe'],
            ['name' => 'Switzerland', 'zone' => 'Europe'],
            ['name' => 'Belgium', 'zone' => 'Europe'],
            ['name' => 'Greece', 'zone' => 'Europe'],
            ['name' => 'Moldova', 'zone' => 'Europe'],
            ['name' => 'Tajikistan', 'zone' => 'Europe'],
            ['name' => 'Bosnia Herzegovina', 'zone' => 'Europe'],
            ['name' => 'Greenland', 'zone' => 'Europe'],
            ['name' => 'Monaco', 'zone' => 'Europe'],            
            ['name' => 'Turkey', 'zone' => 'Europe'],            
            ['name' => 'Bulgaria', 'zone' => 'Europe'],            
            ['name' => 'Hungary', 'zone' => 'Europe'],            
            ['name' => 'Montenegro', 'zone' => 'Europe'],            
            ['name' => 'Turkmenistan', 'zone' => 'Europe'],            
            ['name' => 'Canary Islands', 'zone' => 'Europe'],            
            ['name' => 'Iceland', 'zone' => 'Europe'],            
            ['name' => 'Netherlands', 'zone' => 'Europe'],            
            ['name' => 'Ukraine', 'zone' => 'Europe'],            
            ['name' => 'Corsica', 'zone' => 'Europe'],            
            ['name' => 'Irish Republic', 'zone' => 'Europe'],            
            ['name' => 'Norway', 'zone' => 'Europe'],            
            ['name' => 'Uzbekistan', 'zone' => 'Europe'],            
            ['name' => 'Croatia', 'zone' => 'Europe'],            
            ['name' => 'Italy', 'zone' => 'Europe'],            
            ['name' => 'Poland', 'zone' => 'Europe'],            
            ['name' => 'Vatican City State', 'zone' => 'Europe'],            
            ['name' => 'Cyprus', 'zone' => 'Europe'],            
            ['name' => 'Kazakhstan', 'zone' => 'Europe'],            
            ['name' => 'Portugal', 'zone' => 'Europe'],            
            ['name' => 'Czech Republic', 'zone' => 'Europe'],            
            ['name' => 'Kosovo', 'zone' => 'Europe'],            
            ['name' => 'Romania', 'zone' => 'Europe'],      
            ['name' => 'Canada', 'zone' => 'World Zone 1'],
            ['name' => 'Mexico', 'zone' => 'World Zone 1'],
            ['name' => 'Cuba', 'zone' => 'World Zone 1'],
            ['name' => 'Panama', 'zone' => 'World Zone 1'],
            ['name' => 'Jamaica', 'zone' => 'World Zone 1'],
            ['name' => 'Costa Rica', 'zone' => 'World Zone 1'],
            ['name' => 'Guatemala', 'zone' => 'World Zone 1'],
            ['name' => 'Honduras', 'zone' => 'World Zone 1'],
            ['name' => 'Haiti', 'zone' => 'World Zone 1'],
            ['name' => 'Dominican Republic', 'zone' => 'World Zone 1'],
            ['name' => 'Puerto Rico', 'zone' => 'World Zone 1'],
            ['name' => 'The Bahamas', 'zone' => 'World Zone 1'],
            ['name' => 'Belize', 'zone' => 'World Zone 1'],
            ['name' => 'Barbados', 'zone' => 'World Zone 1'],
            ['name' => 'Cayman Islands', 'zone' => 'World Zone 1'],
            ['name' => 'El Salvador', 'zone' => 'World Zone 1'],
            ['name' => 'Dominica', 'zone' => 'World Zone 1'],
            ['name' => 'Nicaragua', 'zone' => 'World Zone 1'],
            ['name' => 'Monserrat', 'zone' => 'World Zone 1'],
            ['name' => 'Martinique', 'zone' => 'World Zone 1'],
            ['name' => 'Grenada', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Pierre and Miquelon', 'zone' => 'World Zone 1'],
            ['name' => 'Sint Maarten', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Martin', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Kitts and Nevis', 'zone' => 'World Zone 1'],
            ['name' => 'Turks and Caicos Islands', 'zone' => 'World Zone 1'],
            ['name' => 'Antigua and Barbuda', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Lucia', 'zone' => 'World Zone 1'],
            ['name' => 'Guadaloupe', 'zone' => 'World Zone 1'],
            ['name' => 'US Virgin Islands', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Barthelemy', 'zone' => 'World Zone 1'],
            ['name' => 'British Virgin Islands', 'zone' => 'World Zone 1'],
            ['name' => 'Anguilla', 'zone' => 'World Zone 1'],
            ['name' => 'Saint Vincent and the Grenadines', 'zone' => 'World Zone 1'],
            ['name' => 'United States Minor Outlying Islands', 'zone' => 'World Zone 1'],
            ['name' => 'Brazil', 'zone' => 'World Zone 1'],
            ['name' => 'Argentina', 'zone' => 'World Zone 1'],
            ['name' => 'Columbia', 'zone' => 'World Zone 1'],
            ['name' => 'Peru', 'zone' => 'World Zone 1'],
            ['name' => 'Chile', 'zone' => 'World Zone 1'],
            ['name' => 'Bolivia', 'zone' => 'World Zone 1'],
            ['name' => 'Ecuador', 'zone' => 'World Zone 1'],
            ['name' => 'Venezuela', 'zone' => 'World Zone 1'],
            ['name' => 'Guyana', 'zone' => 'World Zone 1'],
            ['name' => 'Suriname', 'zone' => 'World Zone 1'],
            ['name' => 'Uruguay', 'zone' => 'World Zone 1'],
            ['name' => 'Paraguay', 'zone' => 'World Zone 1'],
            ['name' => 'French Guiana', 'zone' => 'World Zone 1'],
            ['name' => 'Trinidad and Tobego', 'zone' => 'World Zone 1'],
            ['name' => 'Aruba', 'zone' => 'World Zone 1'],
            ['name' => 'Falkland Islands', 'zone' => 'World Zone 1'],
            ['name' => 'Curacao', 'zone' => 'World Zone 1'],
            ['name' => 'Caribbean Netherlands', 'zone' => 'World Zone 1'],
            ['name' => 'South Africa', 'zone' => 'World Zone 1'],
            ['name' => 'Nigeria', 'zone' => 'World Zone 1'],
            ['name' => 'Kenya', 'zone' => 'World Zone 1'],
            ['name' => 'Ghana', 'zone' => 'World Zone 1'],
            ['name' => 'Ethiopia', 'zone' => 'World Zone 1'],
            ['name' => 'Democratic Republic of the Congo', 'zone' => 'World Zone 1'],
            ['name' => 'Tanzania', 'zone' => 'World Zone 1'],
            ['name' => 'Morocco', 'zone' => 'World Zone 1'],
            ['name' => 'Senegal', 'zone' => 'World Zone 1'],
            ['name' => 'Uganda', 'zone' => 'World Zone 1'],
            ['name' => 'Mali', 'zone' => 'World Zone 1'],
            ['name' => 'Madagascar', 'zone' => 'World Zone 1'],
            ['name' => 'Zambia', 'zone' => 'World Zone 1'],
            ['name' => 'Cote d\'Ivoire', 'zone' => 'World Zone 1'],
            ['name' => 'Libya', 'zone' => 'World Zone 1'],
            ['name' => 'Angola', 'zone' => 'World Zone 1'],
            ['name' => 'Cameroon', 'zone' => 'World Zone 1'],
            ['name' => 'Somalia', 'zone' => 'World Zone 1'],
            ['name' => 'Algeria', 'zone' => 'World Zone 1'],
            ['name' => 'Zimbabwe', 'zone' => 'World Zone 1'],
            ['name' => 'Rwanda', 'zone' => 'World Zone 1'],
            ['name' => 'Sudan', 'zone' => 'World Zone 1'],
            ['name' => 'Namibia', 'zone' => 'World Zone 1'],
            ['name' => 'Liberia', 'zone' => 'World Zone 1'],
            ['name' => 'Tunisia', 'zone' => 'World Zone 1'],
            ['name' => 'Niger', 'zone' => 'World Zone 1'],
            ['name' => 'Mozambique', 'zone' => 'World Zone 1'],
            ['name' => 'Central African Republic', 'zone' => 'World Zone 1'],
            ['name' => 'Mauritania', 'zone' => 'World Zone 1'],
            ['name' => 'Guinea', 'zone' => 'World Zone 1'],
            ['name' => 'Malawi', 'zone' => 'World Zone 1'],
            ['name' => 'Botswana', 'zone' => 'World Zone 1'],
            ['name' => 'Burkino Faso', 'zone' => 'World Zone 1'],
            ['name' => 'The Gambia', 'zone' => 'World Zone 1'],
            ['name' => 'Cape Verde', 'zone' => 'World Zone 1'],
            ['name' => 'Mauritius', 'zone' => 'World Zone 1'],
            ['name' => 'Benin', 'zone' => 'World Zone 1'],
            ['name' => 'Gabon', 'zone' => 'World Zone 1'],
            ['name' => 'Chad', 'zone' => 'World Zone 1'],
            ['name' => 'Togo', 'zone' => 'World Zone 1'],
            ['name' => 'Djibouti', 'zone' => 'World Zone 1'],
            ['name' => 'Sierra Leone', 'zone' => 'World Zone 1'],
            ['name' => 'Western Sahara', 'zone' => 'World Zone 1'],
            ['name' => 'Eritrea', 'zone' => 'World Zone 1'],
            ['name' => 'South Sudan', 'zone' => 'World Zone 1'],
            ['name' => 'Seychelles', 'zone' => 'World Zone 1'],
            ['name' => 'Burundi', 'zone' => 'World Zone 1'],
            ['name' => 'Eswatini', 'zone' => 'World Zone 1'],
            ['name' => 'Lesotho', 'zone' => 'World Zone 1'],
            ['name' => 'Equatorial Guinea', 'zone' => 'World Zone 1'],
            ['name' => 'Republic of the Congo', 'zone' => 'World Zone 1'],
            ['name' => 'Bahrain', 'zone' => 'World Zone 1'],
            ['name' => 'Egypt', 'zone' => 'World Zone 1'],
            ['name' => 'Iran', 'zone' => 'World Zone 1'],
            ['name' => 'Iraq', 'zone' => 'World Zone 1'],
            ['name' => 'Israel', 'zone' => 'World Zone 1'],
            ['name' => 'Jordan', 'zone' => 'World Zone 1'],
            ['name' => 'Kuwait', 'zone' => 'World Zone 1'],
            ['name' => 'Lebanon', 'zone' => 'World Zone 1'],
            ['name' => 'Oman', 'zone' => 'World Zone 1'],
            ['name' => 'Palestine', 'zone' => 'World Zone 1'],
            ['name' => 'Qatar', 'zone' => 'World Zone 1'],
            ['name' => 'Saudi Arabia', 'zone' => 'World Zone 1'],
            ['name' => 'Syria', 'zone' => 'World Zone 1'],
            ['name' => 'The United Arab Emirates', 'zone' => 'World Zone 1'],
            ['name' => 'Yemen', 'zone' => 'World Zone 1'],
            ['name' => 'China', 'zone' => 'World Zone 1'],
            ['name' => 'Hong Kong', 'zone' => 'World Zone 1'],
            ['name' => 'Macau', 'zone' => 'World Zone 1'],
            ['name' => 'Japan', 'zone' => 'World Zone 1'],
            ['name' => 'North Korea', 'zone' => 'World Zone 1'],
            ['name' => 'South Korea', 'zone' => 'World Zone 1'],
            ['name' => 'Mongolia', 'zone' => 'World Zone 1'],
            ['name' => 'Siberia', 'zone' => 'World Zone 1'],
            ['name' => 'Taiwan', 'zone' => 'World Zone 1'],
            ['name' => 'Brunei', 'zone' => 'World Zone 1'],
            ['name' => 'Cambodia', 'zone' => 'World Zone 1'],
            ['name' => 'East Timor', 'zone' => 'World Zone 1'],
            ['name' => 'Malaysia', 'zone' => 'World Zone 1'],
            ['name' => 'Laos', 'zone' => 'World Zone 1'],
            ['name' => 'Indonesia', 'zone' => 'World Zone 1'],
            ['name' => 'Myanmar', 'zone' => 'World Zone 1'],
            ['name' => 'Singapore', 'zone' => 'World Zone 1'],
            ['name' => 'Philippines', 'zone' => 'World Zone 1'],
            ['name' => 'Thailand', 'zone' => 'World Zone 1'],
            ['name' => 'Vietnam', 'zone' => 'World Zone 1'],
            ['name' => 'Australia', 'zone' => 'World Zone 2'],            
            ['name' => 'Belau', 'zone' => 'World Zone 2'],            
            ['name' => 'British Indian Ocean Territory', 'zone' => 'World Zone 2'],            
            ['name' => 'Christmas Island (Indian Ocean)', 'zone' => 'World Zone 2'],            
            ['name' => 'Christmas Island (Pacific Ocean)', 'zone' => 'World Zone 2'],            
            ['name' => 'Cocos Islands', 'zone' => 'World Zone 2'],            
            ['name' => 'Cook Island', 'zone' => 'World Zone 2'],            
            ['name' => 'Coral Sea Island', 'zone' => 'World Zone 2'],            
            ['name' => 'Fiji', 'zone' => 'World Zone 2'],            
            ['name' => 'French Polynesia', 'zone' => 'World Zone 2'],            
            ['name' => 'French South Antarctic Territory', 'zone' => 'World Zone 2'],            
            ['name' => 'Keeling', 'zone' => 'World Zone 2'],            
            ['name' => 'Kiribati', 'zone' => 'World Zone 2'],            
            ['name' => 'Macao', 'zone' => 'World Zone 2'],            
            ['name' => 'Nauru Island', 'zone' => 'World Zone 2'],            
            ['name' => 'New Caledonia', 'zone' => 'World Zone 2'],            
            ['name' => 'New Zealand', 'zone' => 'World Zone 2'],            
            ['name' => 'New Zealand Antarctic Territory', 'zone' => 'World Zone 2'],            
            ['name' => 'Niue Island', 'zone' => 'World Zone 2'],            
            ['name' => 'Norfolk Island', 'zone' => 'World Zone 2'],            
            ['name' => 'Norwegian Antarctic Territory', 'zone' => 'World Zone 2'],            
            ['name' => 'Papua New Guinea', 'zone' => 'World Zone 2'],            
            ['name' => 'People\'s Democratic Republic of Laos', 'zone' => 'World Zone 2'],            
            ['name' => 'Pitcairn Island', 'zone' => 'World Zone 2'],            
            ['name' => 'Republic of Singapore', 'zone' => 'World Zone 2'],            
            ['name' => 'Solomon Islands', 'zone' => 'World Zone 2'],            
            ['name' => 'Tahiti', 'zone' => 'World Zone 2'],            
            ['name' => 'Tokelau Islands', 'zone' => 'World Zone 2'],            
            ['name' => 'Tonga', 'zone' => 'World Zone 2'],            
            ['name' => 'Tuvalu', 'zone' => 'World Zone 2'],            
            ['name' => 'US Samoa', 'zone' => 'World Zone 2'],            
            ['name' => 'Western Samoa', 'zone' => 'World Zone 2'],            
            ['name' => 'USA', 'zone' => 'World Zone 3'],            
        ];

        foreach($data as $key => $country)
        {
            DeliveryCountry::create([
                'name' => $country['name'],
                'zone' => $country['zone'],
                'sort' => $key
            ]);
        }
    }
}
