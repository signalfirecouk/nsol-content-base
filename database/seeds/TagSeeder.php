<?php

use Illuminate\Database\Seeder;

use App\Models\Tag;
use App\Models\Post;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, rand(10, 100))->states('meta', 'active')->create();

        $posts = Post::all();

        foreach($posts as $post)
        {
            $tags = Tag::live()->inRandomOrder()->limit(rand(0,10))->pluck('id')->toArray();

            if (count($tags) > 0) {
                $post->tags()->sync($tags);
            }
        }
    }
}
