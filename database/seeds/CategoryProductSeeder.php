<?php

use Illuminate\Database\Seeder;

use App\Models\Product;
use App\Models\Category;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        foreach($products as $product)
        {
            $categories = Category::inRandomOrder()
                ->limit(rand(0,10))
                ->pluck('id')
                ->toArray();
            
            if (count($categories) > 0) {
                $product->categories()->sync($categories);
            }
        }    }
}
