<?php

use Illuminate\Database\Seeder;

use App\Models\Picture;
use App\Models\Post;

class PicturePostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();

        foreach($posts as $post)
        {
            $pictures = Picture::inRandomOrder()
                ->limit(rand(0,10))
                ->pluck('id')
                ->toArray();
            
            if (count($pictures) > 0) {
                $post->pictures()->sync($pictures);
            }
        }
    }
}
