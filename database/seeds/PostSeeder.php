<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach($users as $user)
        {
            $total = rand(0, 10);

            if ($total > 0){
                factory(Post::class, $total)->states('meta', 'live')->create([
                    'user_id' => $user->id
                ]);
            }
        }
    }
}
