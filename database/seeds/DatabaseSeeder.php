<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(PictureSeeder::class);
        $this->call(PicturePostSeeder::class);
        $this->call(FileSeeder::class);
        $this->call(FilePostSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(BlockSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(CategoryProductSeeder::class);
        $this->call(PictureProductSeeder::class);
        $this->call(OptionPictureSeeder::class);
        $this->call(TaxSeeder::class);
        $this->call(OptionTaxSeeder::class);
        $this->call(TabSeeder::class);
        $this->call(DeliveryCountrySeeder::class);
        $this->call(DeliveryRateSeeder::class);
        $this->call(StoreSeeder::class);
    }
}
