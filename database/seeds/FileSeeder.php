<?php

use Illuminate\Database\Seeder;

use App\Models\File;
use App\Models\User;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach($users as $user)
        {
            $total = rand(1, 10);

            factory(File::class, $total)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
