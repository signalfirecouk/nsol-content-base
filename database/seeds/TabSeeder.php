<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;

use App\Models\Product;
use App\Models\Tab;

class TabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        $role = Role::findByName('Merchant');       
        $user = $role->users()->first();

        foreach($products as $product) {
            factory(Tab::class, rand(1,10))->states('live')->create([
                'user_id' => $user->id,
                'product_id' => $product->id
            ]);
        }
    }
}
