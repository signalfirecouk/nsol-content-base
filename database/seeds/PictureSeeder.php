<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Picture;

class PictureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach($users as $user)
        {
            $total = rand(1, 10);

            factory(Picture::class, $total)->create([
                'user_id' => $user->id
            ]);
        }
    }
}
