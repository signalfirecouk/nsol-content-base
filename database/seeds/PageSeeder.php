<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();

        $total = rand(5, 20);

        if ($total > 0){
            factory(Page::class, $total)->states('meta', 'live')->create([
                'user_id' => $user->id
            ]);
        }
    }
}
