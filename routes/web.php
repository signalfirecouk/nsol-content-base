<?php

use Illuminate\Support\Facades\Route;

use App\Http\Middleware\CheckSiteEcommerceEnabled;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware([Spatie\Csp\AddCspHeaders::class])->group(function () {

    Auth::routes();

    Route::get('/', 'HomeController')->name('home');

    Route::prefix('blog')->group(function () {
        Route::get('/', 'PostController@index')->name('posts');
        Route::get('/search', 'PostController@search')->name('posts.search');
        Route::get('/tag/{slug}', 'PostController@tag')->name('posts.tag');
        Route::get('/author/{slug}', 'PostController@author')->name('posts.author');
        Route::get('/{slug}', 'PostController@single')->name('posts.single');
    });

    Route::get('/page/{slug}', 'PageController')->name('page');
    
    Route::prefix('contact')->group(function () {
        Route::get('/', 'ContactController@index')->name('contact');
        Route::post('/', 'ContactController@store')->name('contact.store');
        Route::get('/thanks', 'ContactController@thanks')->name('contact.thanks');
    });
    
    Route::get('/image/{width}/{height}/{filename}', 'ImageController')
        ->where('width', '[\d]{2,3}')
        ->where('height', '[\d]{2,3}')
        ->where('filename', '^[A-Za-z0-9]+\.(?:jpg|png|jpeg)$')
        ->name('image');
    
    Route::get('/download/{id}', 'DownloadController')->name('download');
    
    Route::prefix('shop')->middleware([CheckSiteEcommerceEnabled::class])->group(function () {
        Route::get('/', 'Ecommerce\HomeController@index')->name('ecommerce');
        Route::prefix('basket')->group(function () {
            Route::get('/', 'Ecommerce\BasketController@index')->name('ecommerce.basket');
            Route::get('/add/{id}/{quantity}', 'Ecommerce\BasketController@addViaGet')
                ->name('ecommerce.basket.add.get');
            Route::post('/add', 'Ecommerce\BasketController@addViaPost')->name('ecommerce.basket.add.post');
            Route::get('/increase/{id}', 'Ecommerce\BasketController@increase')->name('ecommerce.basket.increase');
            Route::get('/decrease/{id}', 'Ecommerce\BasketController@decrease')->name('ecommerce.basket.decrease');
            Route::get('/remove/{id}', 'Ecommerce\BasketController@remove')->name('ecommerce.basket.remove');
            Route::get('/clear', 'Ecommerce\BasketController@clear')->name('ecommerce.basket.clear');
        });
        Route::get('/category/{slug}', 'Ecommerce\HomeController@category')->name('ecommerce.category');
        Route::get('/product/{slug}', 'Ecommerce\HomeController@single')->name('ecommerce.single');
        Route::prefix('stripe')->group(function () {
            Route::post('/webhook', 'Ecommerce\StripeController@webhook')->name('ecommerce.stripe.webhook');
        });
        Route::get('/stores', 'Ecommerce\StoreController@index')->name('ecommerce.stores');
    });

    Route::middleware(['auth'])->group(function () {
        Route::prefix('account')->group(function () {
            Route::get('/', 'Account\HomeController')->name('account');
            Route::get('/addresses', 'Account\AddressController@index')->name('account.addresses');
            Route::get('/addresses/add', 'Account\AddressController@create')->name('account.addresses.create');
            Route::post('/addresses/add', 'Account\AddressController@store')->name('account.addresses.store');
            Route::get('/addresses/edit/{id}', 'Account\AddressController@edit')->name('account.addresses.edit');
            Route::post('/addresses/edit/{id}', 'Account\AddressController@update')->name('account.addresses.update');
            Route::get('/address/lookup/{postcode}', 'Account\AddressLookupController')
                ->where('postcode', '^([A-Za-z][A-Ha-hJ-Yj-y]?[0-9][A-Za-z0-9]? ?[0-9][A-Za-z]{2}|[Gg][Ii][Rr] ?0[Aa]{2})$')
                ->name('account.address.lookup');
        });
        Route::prefix('shop')->middleware([CheckSiteEcommerceEnabled::class])->group(function () {
            Route::prefix('checkout')->group(function () {
                Route::get('/', 'Ecommerce\CheckoutController@index')->name('ecommerce.checkout');
                Route::post('/', 'Ecommerce\CheckoutController@store')->name('ecommerce.checkout.store');
                Route::get('/increase/{id}', 'Ecommerce\CheckoutController@increase')
                    ->name('ecommerce.checkout.increase');
                Route::get('/decrease/{id}', 'Ecommerce\CheckoutController@decrease')
                    ->name('ecommerce.checkout.decrease');
                Route::get('/remove/{id}', 'Ecommerce\CheckoutController@remove')->name('ecommerce.checkout.remove');
            });
            Route::prefix('invoice')->group(function () {
                Route::get('/pdf/{id}', 'Ecommerce\InvoicePDFController')->name('ecommerce.invoice.pdf');
            });
            Route::prefix('payment')->group(function () {
                Route::get('/', 'Ecommerce\PaymentController@index')->name('ecommerce.payment');
                Route::get('/redirect', 'Ecommerce\PaymentController@redirect')->name('ecommerce.payment.redirect');
                Route::get('/complete', 'Ecommerce\PaymentController@complete')->name('ecommerce.payment.complete');
            });
            Route::prefix('stripe')->group(function () {
                Route::get('/intent', 'Ecommerce\StripeController@intent')->name('ecommerce.stripe.intent');
            });
        });
    });
});
