<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('shop')->group(function () {
    Route::get('/product/{id}/options', 'Ecommerce\API\ProductController@options')
        ->name('ecommerce.api.options');
    Route::get('/options/{id}/pictures', 'Ecommerce\API\OptionController@pictures')
        ->name('ecommerce.api.option.pictures');
});
