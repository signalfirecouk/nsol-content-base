<?php

namespace Signalfirecouk\NovaPointMapField;

use Laravel\Nova\Fields\Field;

use Laravel\Nova\Http\Requests\NovaRequest;

use Grimzy\LaravelMysqlSpatial\Types\Point;

class NovaPointMapField extends Field
{
    /**
    * The field's component.
    *
    * @var string
    */
    public $component = 'nova-point-map-field';

    protected function fillAttributeFromRequest(
        NovaRequest $request,
        $requestAttribute,
        $model,
        $attribute
    ) {
        if ($request->exists($requestAttribute)) {
            // Create a grimzy/point here
            $location = json_decode($request[$requestAttribute]);
            $model->{$attribute} = new Point(
                $location->lng,
                $location->lat
            );
        }
    }

    public function zoom(int $level)
    {
        return $this->withMeta(['zoom' => $level]);
    }
}
