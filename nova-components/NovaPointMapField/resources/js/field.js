import * as VueGoogleMaps from 'vue2-google-maps';

Nova.booting((Vue, router, store) => {  
  Vue.use(VueGoogleMaps, {
    load: {
      key: process.env.MIX_GOOGLE_MAPS_API_KEY,
      libraries: 'places'
    },
    installComponents: true
  });  
  Vue.component('index-nova-point-map-field', require('./components/IndexField'))
  Vue.component('detail-nova-point-map-field', require('./components/DetailField'))
  Vue.component('form-nova-point-map-field', require('./components/FormField'))
})
