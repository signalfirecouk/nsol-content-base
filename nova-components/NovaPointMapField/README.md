# Laravel Nova Point Map Field

Laravel Nova field uses Google Map to show and allow user to click map to place marker. 

The longitude and latitude data provided by Google Maps is then used to create / update a mysql point field.

* Uses grimzy/laravel-mysql-spatial (v2)
* Uses vue2-google-maps

Requires .env variable MIX_GOOGLE_MAPS_API_KEY set with an api key. Create a .env file alongside the included .example.env

In use...

use Signalfirecouk\NovaPointMapField\NovaPointMapField;

```php
NovaPointMapField::make('name_of_mysql_point_field')
    ->zoom(google_zoom_level);
```