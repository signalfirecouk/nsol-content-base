# Nova Solutions Base

Base Laravel solution using Laravel Nova and offering a blog, contact form and pages as default, but with additional functionality based on the user role e.g. users in role editor will have access to blog posts, pages etc, users in role merchant have access to ecommerce etc. User can be in multiple roles.

This is very much a work in progress. 

To migrate and seed run 

`php artisan migrate`

`php artisan db:seed`

You will need Nova (3.8.2) in the nova directory inside the project in order
to use this solution. 

Four users created in seed, all with password `password`

administrator@example.com

editor@example.com

merchant@example.com

public@example.com

All users except public would be able to access admin live, public would not as restricted by viewNova gate in NovaServiceProvider.

Uses spatie/laravel-csp content security policy to help against xss.

Uses spatie/laravel-permission for roles, permissions.

Uses Sentry.io for logging of error messages.