<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Str;

use Tests\TestCase;

use App\Helpers\BasketHelper;

use App\Models\Option;
use App\Models\Product;
use App\Models\User;
use App\Models\BasketItem;

class BasketHelperTest extends TestCase
{
    use RefreshDatabase;

    public function testClearTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);

        $this->assertDatabaseCount('basket_items', 1);

        BasketHelper::clear($item->identifier);

        $this->assertDatabaseCount('basket_items', 0);
    }

    public function testClearNotIdentifierDoesntDeleteTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);

        $this->assertDatabaseCount('basket_items', 1);

        BasketHelper::clear(Str::uuid());

        $this->assertDatabaseCount('basket_items', 1);
    }

    public function testItemCountTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $count = BasketHelper::itemCount($item->identifier);

        $this->assertEquals($count, 1);
    }

    public function testItemCountBasketEmptyForIdentifierTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $count = BasketHelper::itemCount(Str::uuid());

        $this->assertEquals($count, 0);
    }

    public function testValueAsCurrencyTest()
    {
        $this->assertEquals(BasketHelper::valueAsCurrency(10, 'en_GB', 'GBP'), '£10.00');
        $this->assertEquals(BasketHelper::valueAsCurrency(1000, 'en_GB', 'USD'), 'US$1,000.00');
    }

    public function testValueAsPenceTest()
    {
        $this->assertEquals(BasketHelper::valueAsPence(10.99), 1099);
        $this->assertEquals(BasketHelper::valueAsPence(10), 1000);
        $this->assertEquals(BasketHelper::valueAsPence(0.99), 99);
        $this->assertEquals(BasketHelper::valueAsPence(101.99), 10199);
    }

    public function testPriceTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $price = BasketHelper::price($item);
        $this->assertEquals($price, $option->price);
    }

    public function testPriceSalePriceTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'sale_price' => 12.99
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $price = BasketHelper::price($item);

        $this->assertEquals($price, $option->sale_price);
    }

    public function testSubTotalTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $subtotal = BasketHelper::subtotal($item);

        $this->assertEquals($subtotal, $option->price * $item->quantity);
    }

    public function testSubTotalSalePriceTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'sale_price' => 12.99
        ]);
        $item = factory(BasketItem::class)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $subtotal = BasketHelper::subtotal($item);

        $this->assertEquals($subtotal, $option->sale_price * $item->quantity);
    }

    public function testTotalTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
        ]);
        $items = factory(BasketItem::class, 3)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $total = BasketHelper::total($items);

        $expected_total = 0;

        foreach ($items as $item) {
            $expected_total += $item->option->price * $item->quantity;
        }

        $this->assertEquals($total, $expected_total);
    }

    public function testTotalSalePriceTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'sale_price' => 12.99
        ]);
        $items = factory(BasketItem::class, 3)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $total = BasketHelper::total($items);

        $expected_total = 0;

        foreach ($items as $item) {
            $expected_total += $item->option->sale_price * $item->quantity;
        }

        $this->assertEquals($total, $expected_total);
    }

    public function testTotalExtraTest()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $option = factory(Option::class)->states('live')->create([
            'user_id' => $user->id,
            'product_id' => $product->id,
            'sale_price' => 12.99
        ]);
        $items = factory(BasketItem::class, 3)->create([
            'product_id' => $product->id,
            'option_id' => $option->id,
        ]);
        $total = BasketHelper::total($items, 5.99);

        $expected_total = 0;

        foreach ($items as $item) {
            $expected_total += $item->option->sale_price * $item->quantity;
        }

        $expected_total += 5.99;
        $this->assertEquals($total, $expected_total);
    }

    public function testTotalNoItemsTest()
    {
        $this->assertEquals(BasketHelper::total([]), 0);
    }

    public function testTotalNoItemsButExtraTest()
    {
        $this->assertEquals(BasketHelper::total([], 5), 5);
    }
}
