<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Helpers\FeatureHelper;

class FeatureHelperTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if returns true if key in array.
     *
     * @return void
     */
    public function testIsFeatureEnabledTest()
    {
        $this->assertTrue(FeatureHelper::enabled('ecommerce', ['ecommerce']));
        $this->assertFalse(FeatureHelper::enabled('ecommerce', ['test']));
    }
}
