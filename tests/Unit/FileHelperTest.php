<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;

use Tests\TestCase;

use App\Helpers\FileHelper;

class FileHelperTest extends TestCase
{
    use RefreshDatabase;

        /**
     * Test if filesizeHumanDivisorUnit returns correct values at and around
     * values
     *
     * @return void
     */
    public function testGetFileSizeHumanDivisorUnitTest()
    {
        // Below 1024 (1kb)
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(900));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(900));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(900)[0], 'Kb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(900)[1], 1024);

        // At 1024
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1024));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1024));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1024)[0], 'Kb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1024)[1], 1024);

        //Above 1024, Below 1048576 (1Mb)
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1000000));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1000000));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1000000)[0], 'Kb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1000000)[1], 1024);

        //At 1048576
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1048576));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1048576));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1048576)[0], 'Mb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1048576)[1], 1048576);

        // Above 1048576, Below 1073741824 (1Gb)
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1053741824));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1053741824));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1053741824)[0], 'Mb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1053741824)[1], 1048576);

        // At 1073741824
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1073741824));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1073741824));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1073741824)[0], 'Gb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1073741824)[1], 1073741824);

        // Above 1073741824
        $this->assertIsArray(FileHelper::filesizeHumanDivisorUnit(1080000000));
        $this->assertCount(2, FileHelper::filesizeHumanDivisorUnit(1080000000));
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1080000000)[0], 'Gb');
        $this->assertEquals(FileHelper::filesizeHumanDivisorUnit(1080000000)[1], 1073741824);
    }

    /**
     * Test filesizeHuman function
     *
     * @return void
     */
    public function testGetFileSizeHumanTest()
    {
        // Get test file contents
        $contents = Storage::disk('local')->get('test/test.jpg');

        // Create a fake storage disk
        Storage::fake('test');

        // Put the loaded image into test disk
        Storage::disk('test')->put('test.jpg', $contents);

        $this->assertEquals(FileHelper::filesizeHuman('test.jpg', 'test'), '19.46 Kb');
        $this->assertEquals(FileHelper::filesizeHuman('doesntexist.jpg', 'test'), '0');
    }
}
