<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Models\User;
use App\Models\Block;

use App\Helpers\ContentHelper;

class ContentHelperTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test transforming EOL delimited strings into paragraphs
     *
     * @return void
     */
    public function testNewLineToParagraphTest()
    {
        $string = 'Paragraph 1' . PHP_EOL . 'Paragraph 2';
        $this->assertEquals(ContentHelper::nl2p($string), '<p>Paragraph 1</p>' . PHP_EOL . '<p>Paragraph 2</p>');
    }

    /**
     * Test blockBySlug method
     *
     * @return void
     */
    public function testblockBySlugExistsTest()
    {
        $user = factory(User::class)->create();
        $block = factory(Block::class)->states('live')->create([
            'user_id' => $user->id
        ]);
        $this->assertEquals(ContentHelper::blockBySlug($block->slug), $block->content);
    }

    /**
     * Test blockBySlug where block doesnt exist
     *
     * @return void
     */
    public function testblockBySlugDoesNotExistTest()
    {
        $this->assertEquals(ContentHelper::blockBySlug('testing'), 'Block Not Found');
    }

    /**
     * Test if getFontAwesomeClass returns correct font awesome
     * class based on filename extension
     *
     * @return void
     */
    public function testGetFontAwesomeClassForExtension()
    {
        $this->assertEquals(ContentHelper::getFontAwesomeClass('test.pdf'), 'fas fa-file-pdf');
        $this->assertEquals(ContentHelper::getFontAwesomeClass('test.tst'), 'fas fa-file');
    }
}
