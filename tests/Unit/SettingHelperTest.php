<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Models\Setting;

use App\Helpers\SettingHelper;

class SettingHelperTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if get returns correct value from setting
     *
     * @return void
     */
    public function testGetSettingTest()
    {
        $setting = factory(Setting::class)->create();
        $this->assertEquals(SettingHelper::get($setting->key, null), $setting->value);
    }

    /**
     * Test if get returns default value when key not found
     *
     * @return void
     */
    public function testGetSettingDefaultTest()
    {
        $this->assertEquals(SettingHelper::get('testkeydoesnotexist', 'test'), 'test');
    }
}
