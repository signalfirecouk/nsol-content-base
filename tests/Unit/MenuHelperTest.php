<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Helpers\MenuHelper;
use OptimistDigital\MenuBuilder\Models\Menu;
use OptimistDigital\MenuBuilder\Models\MenuItem;

class MenuHelperTest extends TestCase
{
    use RefreshDatabase;

    public function testMenuItemUrlTest()
    {
        $this->assertEquals(
            MenuHelper::menuItemUrl([
                'type' => 'static-url',
                'value' => '/test',
            ]),
            '/test'
        );
        $this->assertEquals(
            MenuHelper::menuItemUrl(['type' => 'post', 'value' => '/test']),
            '/test'
        );
        $this->assertEquals(
            MenuHelper::menuItemUrl(['type' => 'page', 'value' => '/test']),
            '/test'
        );
        $this->assertEquals(MenuHelper::menuItemUrl(['type' => 'test']), '#');
    }

    public function testMenuItemLinkTest()
    {
        $this->assertEquals(
            MenuHelper::menuItemLink(
                [
                    'name' => 'test',
                    'target' => '_self',
                ],
                '/test'
            ),
            '<a href="/test" target="_self">test</a>'
        );
        $this->assertEquals(
            MenuHelper::menuItemLink(
                [
                    'name' => 'test',
                    'target' => '_self',
                ],
                '/test',
                ['test']
            ),
            '<a href="/test" target="_self" class="test">test</a>'
        );
    }

    public function testMenuItemLinkWithCssClassTest()
    {
        $this->assertEquals(
            MenuHelper::menuItemLink(
                [
                    'name' => 'test',
                    'target' => '_self',
                ],
                '/test',
                ['test']
            ),
            '<a href="/test" target="_self" class="test">test</a>'
        );
    }

    public function testMenuItemsTest()
    {
        $items = [
            [
                'type' => 'static-url',
                'name' => 'Test1',
                'value' => '/test1',
                'target' => '_self',
                'enabled' => true,
            ],
            [
                'type' => 'post',
                'name' => 'Test2',
                'value' => '/test2',
                'target' => '_self',
                'enabled' => true,
            ],
            [
                'type' => 'page',
                'name' => 'Test3',
                'value' => '/test3',
                'target' => '_blank',
                'enabled' => true,
            ],
            [
                'type' => 'test',
                'name' => 'Test4',
                'target' => '_self',
                'enabled' => true,
            ],
            [
                'type' => 'static-url',
                'name' => 'Test5',
                'value' => '/test5',
                'target' => '_self',
                'enabled' => false,
            ],
        ];

        $result = MenuHelper::menuItems($items);

        $this->assertIsArray($result);
        $this->assertCount(6, $result);
        $this->assertEquals($result[0], '<ul>');
        $this->assertEquals(
            $result[1],
            '<li><a href="/test1" target="_self">Test1</a></li>'
        );
        $this->assertEquals(
            $result[2],
            '<li><a href="/test2" target="_self">Test2</a></li>'
        );
        $this->assertEquals(
            $result[3],
            '<li><a href="/test3" target="_blank">Test3</a></li>'
        );
        $this->assertEquals(
            $result[4],
            '<li><a href="#" target="_self">Test4</a></li>'
        );
        $this->assertEquals($result[5], '</ul>');
    }

    public function testMenuItemsCssClassTest()
    {
        $items = [
            [
                'type' => 'static-url',
                'name' => 'Test1',
                'value' => '/test1',
                'target' => '_self',
                'enabled' => true,
            ],
        ];

        $result = MenuHelper::menuItems($items, ['test']);

        $this->assertIsArray($result);
        $this->assertEquals($result[0], '<ul class="test">');
    }

    public function testMenuItemsNavbarTest()
    {
        $items = [
            [
                'type' => 'static-url',
                'name' => 'Test1',
                'value' => '/test1',
                'target' => '_self',
                'enabled' => true,
                'children' => [],
            ],
        ];

        $result = MenuHelper::menuItemsNavBar($items);

        $this->assertIsArray($result);
        $this->assertEquals($result[0], '<li class="nav-item">');
        $this->assertEquals(
            $result[1],
            '<a href="/test1" target="_self" class="nav-link">Test1</a>'
        );
        $this->assertEquals($result[2], '</li>');
    }

    public function testMenuItemsNavbarChildrenTest()
    {
        $items = [
            [
                'type' => 'static-url',
                'name' => 'Test1',
                'value' => '/test1',
                'target' => '_self',
                'enabled' => true,
                'children' => [
                    [
                        'type' => 'static-url',
                        'name' => 'Test2',
                        'value' => '/test2',
                        'target' => '_self',
                        'enabled' => true,
                        'children' => [],
                    ],
                    [
                        'type' => 'static-url',
                        'name' => 'Test3',
                        'value' => '/test3',
                        'target' => '_self',
                        'enabled' => true,
                        'children' => [],
                    ],
                    [
                        'type' => 'static-url',
                        'name' => 'Test4',
                        'value' => '/test4',
                        'target' => '_self',
                        'enabled' => false,
                        'children' => [],
                    ],
                ],
            ],
        ];

        $result = MenuHelper::menuItemsNavBar($items);

        $this->assertIsArray($result);
        $this->assertEquals($result[0], '<li class="nav-item dropdown">');
        $this->assertEquals(
            $result[1],
            '<a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Test1</a>'
        );
        $this->assertEquals($result[2], '<div class="dropdown-menu">');
        $this->assertEquals(
            $result[3],
            '<a href="/test2" target="_self" class="dropdown-item">Test2</a>'
        );
        $this->assertEquals(
            $result[4],
            '<a href="/test3" target="_self" class="dropdown-item">Test3</a>'
        );
        $this->assertEquals($result[5], '</div>');
        $this->assertEquals($result[6], '</li>');
    }

    public function testMenuTest()
    {
        $menu = factory(Menu::class)->create();
        $item1 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 1,
        ]);
        $item2 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 2,
        ]);

        $result = explode(PHP_EOL, MenuHelper::menu($menu->slug));

        $this->assertEquals($result[0], '<ul>');
        $this->assertEquals(
            $result[1],
            '<li><a href="' .
                $item1->value .
                '" target="' .
                $item1->target .
                '">' .
                $item1->name .
                '</a></li>'
        );
        $this->assertEquals(
            $result[2],
            '<li><a href="' .
                $item2->value .
                '" target="' .
                $item2->target .
                '">' .
                $item2->name .
                '</a></li>'
        );
        $this->assertEquals($result[3], '</ul>');
    }

    public function testMenuCssClassTest()
    {
        $menu = factory(Menu::class)->create();
        $result = explode(
            PHP_EOL,
            MenuHelper::menu($menu->slug, 'default', ['test'])
        );
        $this->assertEquals($result[0], '<ul class="test">');
        $this->assertEquals($result[1], '</ul>');
    }

    public function testMenuNavBarTest()
    {
        $menu = factory(Menu::class)->create();
        $item1 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 1,
        ]);
        $item2 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 2,
        ]);

        $result = explode(PHP_EOL, MenuHelper::menu($menu->slug, 'navbar'));

        $this->assertEquals($result[0], '<li class="nav-item">');
        $this->assertEquals(
            $result[1],
            '<a href="' .
                $item1->value .
                '" target="' .
                $item1->target .
                '" class="nav-link">' .
                $item1->name .
                '</a>'
        );
        $this->assertEquals($result[2], '</li>');
        $this->assertEquals($result[3], '<li class="nav-item">');
        $this->assertEquals(
            $result[4],
            '<a href="' .
                $item2->value .
                '" target="' .
                $item2->target .
                '" class="nav-link">' .
                $item2->name .
                '</a>'
        );
        $this->assertEquals($result[5], '</li>');
    }

    public function testMenuNavBarChildrenTest()
    {
        $menu = factory(Menu::class)->create();
        $item1 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 1,
        ]);
        $item2 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'order' => 2,
        ]);
        $item3 = factory(MenuItem::class)->create([
            'menu_id' => $menu->id,
            'parent_id' => $item2->id,
            'order' => 1,
        ]);

        $result = explode(PHP_EOL, MenuHelper::menu($menu->slug, 'navbar'));

        $this->assertEquals($result[0], '<li class="nav-item">');
        $this->assertEquals(
            $result[1],
            '<a href="' .
                $item1->value .
                '" target="' .
                $item1->target .
                '" class="nav-link">' .
                $item1->name .
                '</a>'
        );
        $this->assertEquals($result[2], '</li>');
        $this->assertEquals($result[3], '<li class="nav-item dropdown">');
        $this->assertEquals(
            $result[4],
            '<a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                $item2->name .
                '</a>'
        );
        $this->assertEquals($result[5], '<div class="dropdown-menu">');
        $this->assertEquals(
            $result[6],
            '<a href="' .
                $item3->value .
                '" target="' .
                $item3->target .
                '" class="dropdown-item">' .
                $item3->name .
                '</a>'
        );
        $this->assertEquals($result[7], '</div>');
        $this->assertEquals($result[8], '</li>');
    }

    public function testMenuMissingTest()
    {
        $this->assertNull(MenuHelper::menu('iammissing'));
    }
}
