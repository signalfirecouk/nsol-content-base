<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Models\Page;
use App\Models\Post;
use App\Models\User;

class HomeRoutesAvailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test homepage route response
     *
     * @return void
     */
    public function testHomeRouteTest()
    {
        $response = $this->get(route('home'));
        $response->assertStatus(200);
    }
}
