<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class EcommerceRoutesAvailableTest extends TestCase
{
    use RefreshDatabase;

 /**
     * Test ecommerce home route response
     *
     * @return void
     */
    public function testEcommerceRouteTest()
    {
        $response = $this->get(route('ecommerce'));
        $response->assertStatus(200);
    }
            
    /**
     * Test ecommerce basket route response
     *
     * @return void
     */
    public function testEcommerceBasketRouteTest()
    {
        $response = $this->get(route('ecommerce.basket'));
        $response->assertStatus(200);
    }
}
