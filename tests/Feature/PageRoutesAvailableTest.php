<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Models\Page;
use App\Models\User;

class PageRoutesAvailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test live page route response
     */
    public function testPageRoutePageLiveTest()
    {
        $user = factory(User::class)->create();
        $page = factory(Page::class)->states('meta', 'live')->create([
            'user_id' => $user->id,
        ]);
        $response = $this->get(route('page', ['slug' => $page->slug]));
        $response->assertStatus(200);
    }

    /**
     * Test draft page route response
     */
    public function testPageRoutePageDraftTest()
    {
        $user = factory(User::class)->create();
        $page = factory(Page::class)->states('meta', 'draft')->create([
            'user_id' => $user->id,
        ]);
        $response = $this->get(route('page', ['slug' => $page->slug]));
        $response->assertStatus(404);
    }

    /**
     * Test page route missing page response
     */
    public function testPageRouteMissingPageTest()
    {
        $response = $this->get(route('page', ['slug' => 'ishouldfail']));
        $response->assertStatus(404);
    }
}
