<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

use App\Models\Post;
use App\Models\User;

class PostRoutesAvailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test posts index route response
     *
     * @return void
     */
    public function testPostsRouteTest()
    {
        $response = $this->get(route('posts'));
        $response->assertStatus(200);
    }

    /**
     * Test live post route response
     */
    public function testPostRoutePageLiveTest()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->states('meta', 'live')->create([
            'user_id' => $user->id,
        ]);
        $response = $this->get(route('posts.single', ['slug' => $post->slug]));
        $response->assertStatus(200);
    }

    /**
     * Test draft post route response
     */
    public function testPostRoutePageDraftTest()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->states('meta', 'draft')->create([
            'user_id' => $user->id,
        ]);
        $response = $this->get(route('posts.single', ['slug' => $post->slug]));
        $response->assertStatus(404);
    }

    /**
     * Test post route missing page response
     */
    public function testPostRouteMissingPageTest()
    {
        $response = $this->get(route('posts.single', ['slug' => 'ishouldfail']));
        $response->assertStatus(404);
    }
}
