<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class ContactRoutesAvailableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test contact route response
     *
     * @return void
     */
    public function testContactRouteTest()
    {
        $response = $this->get(route('contact'));
        $response->assertStatus(200);
    }

    /**
     * Test contact thanks route response
     *
     * @return void
     */
    public function testContactThanksRouteTest()
    {
        $response = $this->get(route('contact.thanks'));
        $response->assertStatus(200);
    }
}
